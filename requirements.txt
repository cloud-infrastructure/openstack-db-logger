setuptools
pbr!=2.1.0,>=2.0.0
kazoo
influxdb
sqlalchemy
flake8
keystoneauth1
python-keystoneclient
os-client-config
radosgw-admin
python-octaviaclient
PyYAML
boto3
python-neutronclient
-e git+https://gitlab.cern.ch/cloud-infrastructure/python-neutronclient-cern.git@master#egg=networking_cern_client
