import logging
import sys

from dblogger.consumer.base import Base
import influxdb

logger = logging.getLogger("dblogger")


class InfluxConsumer(Base):

    def __init__(self, queue, configs, section='influxdb'):
        """Initialize influx consumer."""
        super(InfluxConsumer, self).__init__(queue, configs)
        self.client = self.__init_client(section)

    def __init_client(self, section):
        try:
            client = influxdb.InfluxDBClient(
                host=self.configs.get(section, 'host'),
                port=self.configs.get(section, 'port'),
                username=self.configs.get(section, 'user'),
                password=self.configs.get(section, 'pass'),
                ssl=self.configs.getboolean(section, 'ssl'),
                database=self.configs.get(section, 'database'))
        except Exception:
            logger.exception("Error connecting to InfluxDB. Check "
                             "database existance and connection URL")
            sys.exit(-1)

        return client

    def process(self, sample):
        logger.debug("Arrived sample: %s" % sample)

        influx_metric = [{
            'measurement': sample['header']['metric'],
            'tags': {key: value for key, value in sample[
                'header'].items() if key != 'metric'},
            'fields': sample['values']
        }]  # Exclude 'metric' from columnts

        try:
            logger.debug("Inserting: %s" % influx_metric)
            self.client.write_points(influx_metric)
        except Exception:
            logger.exception("Error sending sample to influxdb")
