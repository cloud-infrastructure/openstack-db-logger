import boto3
from botocore.exceptions import ClientError
from dblogger.consumer.backend import base
import logging
import os
import tempfile
import yaml

logger = logging.getLogger("dblogger")


class FileBackend(base.Base):

    def __init__(self, configs, section):
        """Initialize file backend."""
        super(FileBackend, self).__init__(configs, section)
        self.__init_client(section)

    def __init_client(self, section):
        try:
            self.bucket = self.configs.get(section, 'bucket')
            self.client = boto3.client('s3', endpoint_url='https://s3.cern.ch')
        except Exception:
            logger.exception("Error accessing given path")

    def process(self, sample):
        region = sample['region'] if 'region' in sample else 'none'
        logger.debug("Arrived sample: %s for region %s",
                     sample, region)
        file_content = {}
        file_content['input'] = []
        for lbs in sample['values']['data']:
            file_content['input'].append(lbs)

        fd, path = tempfile.mkstemp()
        try:
            with os.fdopen(fd, 'w', encoding='utf-8') as yaml_file:
                yaml.dump(file_content, yaml_file, default_flow_style=False)
            self.client.upload_file(
                path,
                self.bucket,
                f"{region}.yaml"
            )
        except IOError:
            logger.exception("Error writing to file")
        except ClientError as e:
            logging.error(e)
        finally:
            os.remove(path)
