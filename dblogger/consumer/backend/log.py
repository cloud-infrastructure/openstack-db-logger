import logging

from dblogger.consumer.backend import base

logger = logging.getLogger("dblogger")


class LogBackend(base.Base):

    def __init__(self, configs, section):
        """Initialize log backend."""
        super(LogBackend, self).__init__(configs, section)

    def process(self, sample):
        logger.info("consumer.log - %s" % (sample,))
