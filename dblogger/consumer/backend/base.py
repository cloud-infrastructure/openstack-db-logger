import abc
import logging

from configparser import NoOptionError

logger = logging.getLogger("dblogger")


class Base(object):
    """Base class for consumers backends."""

    def __init__(self, configs, section):
        """Initialize base class for consumers."""
        super(Base, self).__init__()
        self.configs = configs
        try:
            self.allowlist = self.configs.get(
                section, 'allowlist').split(',')
        except NoOptionError:
            self.allowlist = None
        try:
            self.denylist = self.configs.get(
                section, 'denylist').split(',')
        except NoOptionError:
            self.denylist = None

    def is_allowed(self, measurement):
        if self.denylist and measurement in self.denylist:
            return False
        elif self.allowlist and measurement not in self.allowlist:
            return False
        else:
            return True

    @abc.abstractmethod
    def process(self, sample):
        pass
