import logging
import multiprocessing

logger = logging.getLogger("dblogger")


class Base(multiprocessing.Process):
    """Base class for consumers."""

    def __init__(self, queue, configs):
        """Init base class for consumers."""
        super(Base, self).__init__()
        self.daemon = True
        self.queue = queue
        self.configs = configs

    def run(self):
        # Main loop
        while True:
            try:
                sample = self.queue.get()
                self.process(sample)
            except Exception:
                logger.exception("Exception reading from the queue")

    def process(self, sample):
        pass
