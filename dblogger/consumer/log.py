import logging

from dblogger.consumer.base import Base

logger = logging.getLogger("dblogger")


class LogConsumer(Base):

    def __init__(self, queue, configs):
        """Initialize log consumer."""
        super(LogConsumer, self).__init__(queue, configs)

    def process(self, sample):
        logger.info("consumer.log - %s" % (sample,))
