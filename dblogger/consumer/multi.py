import logging

from dblogger.consumer.base import Base
from stevedore.driver import DriverManager

logger = logging.getLogger("dblogger")


class MultiConsumer(Base):

    def __init__(self, queue, configs):
        """Initialize multi backend consumer."""
        super(MultiConsumer, self).__init__(queue, configs)
        self.backends = []
        for consumer in self.configs.get('dblogger', 'consumers').split(','):
            backend_type = self.configs.get(consumer, 'type')
            # File backends are skipped to avoid duplicate processing.
            self.backends.append(
                DriverManager(
                    namespace='dblogger.consumer_backends',
                    name=backend_type,
                    invoke_on_load=True,
                    invoke_args=(self.configs, consumer,),
                ).driver
            )

    def process(self, sample):
        logger.debug("Arrived sample: %s" % sample)
        for backend in self.backends:
            if backend.is_allowed(sample['header']['metric']):
                backend.process(sample)
