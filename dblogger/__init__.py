import logging
import multiprocessing
import signal
import sys
import time

from configparser import NoOptionError
from dblogger.helpers.cache import CacheManager
from dblogger.helpers.cache import CacheThread
from kazoo import client as kazooclient
from stevedore.driver import DriverManager

logger = logging.getLogger("dblogger")


def init_producer(queue, cache):
    run_producer.queue = queue
    run_producer.projects = cache


def run_producer(section, configs):
    try:
        DriverManager(
            namespace='dblogger.producer',
            name=configs.get(section, 'type'),
            invoke_on_load=True,
            invoke_args=(
                run_producer.queue,
                section,
                configs,
                run_producer.projects),
        ).driver.run()
    except Exception:
        logger.exception("Error retrieving sample")


class DBLogger(object):

    def __init__(self, configs):
        """Init main."""
        super(DBLogger, self).__init__()
        self.configs = configs

    def run(self):
        mgr = CacheManager()
        mgr.start()

        # Queue where the samples are stored
        logger.info("Creating queue")
        self.queue = multiprocessing.Queue(maxsize=0)

        self.cache = mgr.Cache('keystone', self.configs, )
        self.cache_thread = CacheThread(self.cache)

        logger.info("Creating keystone cache process")
        self.cache_thread.start()

        # Create a pool of producers
        num_producers = int(self.configs.get('dblogger', 'workers'))
        logger.info("Creating producer pool with {} workers".format(
            num_producers
        ))
        self.pool = multiprocessing.Pool(
            processes=num_producers,
            initializer=init_producer,
            initargs=(self.queue, self.cache, ))

        # Start consumers
        try:
            consumer_type = self.configs.get('dblogger', 'consumer_type')
        except NoOptionError:
            consumer_type = 'influx'
        num_consumers = multiprocessing.cpu_count()
        logger.info('Creating {} consumers'.format(num_consumers))
        self.consumers = [
            DriverManager(
                namespace='dblogger.consumer',
                name=consumer_type,
                invoke_on_load=True,
                invoke_args=(self.queue, self.configs,),
            ).driver for i in range(num_consumers)
        ]

        for consumer in self.consumers:
            consumer.start()

        # Configure signal handling
        signal.signal(signal.SIGINT, self.terminate)
        signal.signal(signal.SIGTERM, self.terminate)
        signal.signal(signal.SIGQUIT, self.terminate)

        try:
            sections_to_remove = self.configs.get(
                'dblogger', 'consumers').split(',')
        except NoOptionError:
            sections_to_remove = []

        # Get list of sections
        sections = list(
            set(self.configs.sections())
            - set(['dblogger'])
            - set(sections_to_remove))

        # Configure kazoo
        logger.info("Creating kazoo partitioner")
        self.kclient = kazooclient.KazooClient(
            hosts=self.configs.get('dblogger', 'zookeeper_hosts'),
            logger=logger)
        self.kclient.start()

        partitioner = self.kclient.SetPartitioner(
            path=self.configs.get('dblogger', 'zookeeper_path'),
            set=tuple(sections))

        # Initial timeout to avoid double accounting
        logger.info("Waiting interval to avoid double counting metrics")
        time.sleep(int(self.configs.get('dblogger', 'interval')))

        # Run producers loop
        while True:
            if partitioner.failed:
                raise Exception("Lost or unable to acquire partition")
            elif partitioner.release:
                partitioner.release_set()
            elif partitioner.acquired:
                for section in partitioner:
                    logger.info("Running polling task for %s" % section)
                    self.pool.apply_async(run_producer,
                                          args=(section, self.configs,))
                # Wait for next task run
                time.sleep(int(self.configs.get('dblogger', 'interval')))
            elif partitioner.allocating:
                partitioner.wait_for_acquire()

    def terminate(self, signal, frame):
        logger.info("Terminating kazoo partitioner")
        self.kclient.stop()

        logger.info("Closing the queue")
        self.queue.close()

        logger.info("Terminating keystone cache")
        self.cache_thread.terminate()

        logger.info("Terminating consumers")
        for consumer in self.consumers:
            consumer.terminate()

        logger.info("Terminating producer pool")
        self.pool.terminate()

        sys.exit(0)
