#!/usr/bin/python3

import argparse
import configparser
import logging
import logging.config
import sys
import time
import traceback
import urllib3
import yaml

from dblogger import DBLogger

# configure logging
with open('/etc/dblogger/logging.yml', 'r') as stream:
    config = yaml.safe_load(stream)
logging.config.dictConfig(config)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
logging.Formatter.converter = time.gmtime


class DBloggerCMD(object):
    def __init__(self):
        """Init command wrapper."""
        self.parser = argparse.ArgumentParser(description="dblogger")

        self.parser.add_argument(
            '-c',
            '--config',
            default='/etc/dblogger/dblogger.conf',
            help='Configuration file for dblogger'
        )

    def main(self, args=None):
        args = self.parser.parse_args(args)

        config = configparser.ConfigParser()
        config.read(args.config)

        dblogger = DBLogger(config)
        dblogger.run()


# Needs static method for setup.cfg
def main(args=None):
    DBloggerCMD().main(args)


if __name__ == "__main__":
    try:
        main()
    except Exception:
        traceback.print_exc(file=sys.stdout)
        sys.exit(-1)
