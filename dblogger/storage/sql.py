
import uuid

from sqlalchemy import create_engine, inspect, MetaData, Table
from sqlalchemy.orm import mapper, sessionmaker
from sqlalchemy.pool import NullPool


class SQL(object):

    def __init__(self, connection):
        """Initialize sql storage."""
        super(SQL, self).__init__()

        self.engine = create_engine(
            connection, poolclass=NullPool)

        self.metadata = MetaData(self.engine)

        Session = sessionmaker(bind=self.engine)
        self.session = Session(autocommit=True)

    def get_db_tables(self):
        inspector = inspect(self.engine)
        return inspector.get_table_names()

    def map_table(self, table):
        tbl = Table(table, self.metadata, autoload=True)
        cls_name = "%s_%s" % (table, str(uuid.uuid4()))
        cls = type(cls_name, (object, ), {})
        mapper(cls, tbl)
        return cls

    def query(self, *args):
        try:
            return self.session.query(*args)
        except Exception:
            self.session.rollback()
            raise

    def close(self):
        self.session.close()
