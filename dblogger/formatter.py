from logging import Formatter
import time


class GMTFormatter(Formatter):
    converter = time.gmtime
