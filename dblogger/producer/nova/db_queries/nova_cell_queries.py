import datetime
import logging
from sqlalchemy import func

logger = logging.getLogger("dblogger")


class NovaCellDBQueries:
    def __init__(self, db):
        """Initialize nova cell db queries."""
        self.db = db
        self.ComputeNode = self.db.map_table('compute_nodes')
        self.Service = self.db.map_table('services')
        self.Instance = self.db.map_table('instances')

    def hv_total(self):
        # SELECT COUNT(*) FROM nova.services WHERE topic = "compute"
        # AND NOT deleted;
        return (
            self.db.query(
                func.count(self.Service.id).label("hv_total"))  # noqa: E126
            .filter(self.Service.topic == "compute")
            .filter(self.Service.deleted == 0).scalar())

    def hv_total_per_processor(self):
        # SELECT COUNT(*) AS count,
        #        SUM(memory_mb) AS memory,
        #        cpu_info -> "$.vendor" AS vendor,
        #        cpu_info -> "$.model" AS model
        # FROM nova.compute_nodes WHERE hypervisor_hostname IN
        #     (SELECT host FROM nova.services
        #      WHERE topic = "compute" AND NOT deleted)
        # GROUP BY vendor, model
        return (
            self.db.query(
                func.count(self.ComputeNode.id).label("count"),  # noqa: E126
                func.sum(self.ComputeNode.memory_mb).label("memory"),
                func.json_extract(self.ComputeNode.cpu_info,
                                  "$.vendor").label("processor"),
                func.json_extract(self.ComputeNode.cpu_info,
                                  "$.model").label("model"))
            .filter(
                self.Service.host.ilike(
                    self.ComputeNode.hypervisor_hostname.concat('%')))
            .filter(self.Service.topic == "compute")
            .filter(self.Service.deleted == 0)
            .group_by(
                func.json_extract(self.ComputeNode.cpu_info,
                                  "$.vendor").label("processor"),
                func.json_extract(self.ComputeNode.cpu_info,
                                  "$.model").label("model")
            ).all()
        )

    def hv_disabled(self):
        # SELECT COUNT(*) FROM nova.services
        # WHERE topic = "compute" AND NOT deleted AND disabled > 0;
        return (
            self.db.query(
                func.count(self.Service.id).label("hv_disabled"))  # noqa: E126
            .filter(self.Service.topic == "compute")
            .filter(self.Service.deleted == 0)
            .filter(self.Service.disabled > 0).scalar())

    def hv_usage(self):
        hv_usage = {}

        # hv_cpu_total
        # hv_cpu_used
        # hv_ram_total
        # hv_ram_used
        # hv_disk_total
        # hv_disk_used
        # SELECT SUM(vcpus) AS vcpu, SUM(vcpus_used) AS vcpu_used,
        #        SUM(memory_mb) AS ram, SUM(memory_mb_used) AS ram_used,
        #        SUM(local_gb) AS disk, SUM(local_gb_used) AS disk_used
        # FROM nova.compute_nodes WHERE hypervisor_hostname IN
        #     (SELECT host FROM nova.services
        #      WHERE topic = "compute" AND NOT deleted);
        hv_tmp = (
            self.db.query(
                func.sum(self.ComputeNode.vcpus).label("hv_cpu_total"),  # noqa
                func.sum(self.ComputeNode.vcpus_used).label("hv_cpu_used"),
                func.sum(self.ComputeNode.memory_mb).label("hv_ram_total"),
                func.sum(self.ComputeNode.memory_mb_used).label("hv_ram_used"),
                func.sum(self.ComputeNode.local_gb).label("hv_disk_total"),
                func.sum(self.ComputeNode.local_gb_used).label("hv_disk_used"))
            .filter(
                self.Service.host.ilike(
                    self.ComputeNode.hypervisor_hostname.concat('%')))
            .filter(self.Service.topic == "compute")
            .filter(self.Service.deleted == 0).one())

        hv_usage["hv_cpu_total"] = hv_tmp.hv_cpu_total or 0
        hv_usage["hv_cpu_used"] = hv_tmp.hv_cpu_used or 0
        hv_usage["hv_ram_total"] = hv_tmp.hv_ram_total or 0
        hv_usage["hv_ram_used"] = hv_tmp.hv_ram_used or 0
        hv_usage["hv_disk_total"] = hv_tmp.hv_disk_total or 0
        hv_usage["hv_disk_used"] = hv_tmp.hv_disk_used or 0

        return {k: int(v) for k, v in hv_usage.items()}

    def hv_detail(self):
        # SELECT cn.hypervisor_hostname AS hostname,
        #        cn.vcpus AS hv_cores, cn.vcpus_used AS vm_cores,
        #        cn.memory_mb AS hv_ram, cn.memory_mb_used AS vm_ram,
        #        cn.local_gb AS hv_disk, cn.local_gb_used AS vm_disk,
        #        cn.running_vms AS vm_count,
        #        cn.hypervisor_version AS hv_version,
        #        s.disabled AS hv_disabled
        # FROM nova.compute_nodes cn
        # JOIN nova.services s ON cn.hypervisor_hostname = s.host
        # WHERE cn.hypervisor_hostname IN
        #      (SELECT host FROM nova.services
        #       WHERE topic = "compute" AND NOT deleted);
        return (
            self.db.query(
                self.ComputeNode.hypervisor_hostname.label(
                    "hostname"),  # noqa
                self.ComputeNode.vcpus.label("hv_cores"),
                self.ComputeNode.vcpus_used.label("vm_cores"),
                self.ComputeNode.memory_mb.label("hv_ram"),
                self.ComputeNode.memory_mb_used.label("vm_ram"),
                self.ComputeNode.local_gb.label("hv_disk"),
                self.ComputeNode.local_gb_used.label("vm_disk"),
                self.ComputeNode.running_vms.label("vm_count"),
                self.ComputeNode.hypervisor_version.label("hv_version"),
                self.Service.disabled.label("hv_disabled"))
            .join(self.Service, self.Service.host
                  == self.ComputeNode.hypervisor_hostname)
            .filter(self.Service.topic == "compute")
            .filter(self.Service.deleted == 0).all()
        )

    def vms_per_status(self):
        # SELECT COUNT(*), vm_state FROM nova.instances WHERE NOT deleted
        # GROUP BY vm_state;
        return (
            self.db.query(
                func.count(self.Instance.id),  # noqa: E126
                self.Instance.vm_state)
            .filter(self.Instance.deleted == 0)
            .group_by(self.Instance.vm_state).all())

    def vms_per_status_per_project(self):
        # SELECT COUNT(*), vm_state, project_id FROM nova.instances
        # WHERE NOT deleted
        # GROUP BY vm_state, project_id;
        return (
            self.db.query(
                func.count(self.Instance.id).label("count"),  # noqa: E126
                self.Instance.vm_state.label("status"),
                self.Instance.project_id)
            .filter(self.Instance.deleted == 0)
            .group_by(
                self.Instance.vm_state,
                self.Instance.project_id
            ).all()
        )

    def vm_usage_per_project_per_cell(self):
        # SELECT project_id, SUM(vcpus),
        #        SUM(memory_mb), COUNT(id) AS instances
        # FROM instances
        # WHERE NOT deleted
        # GROUP BY project_id;
        return (
            self.db.query(
                self.Instance.project_id,
                func.sum(self.Instance.vcpus).label("cores"),
                func.sum(self.Instance.memory_mb).label("ram"),
                func.count(self.Instance.id).label("instances"))
            .filter(self.Instance.deleted == 0)
            .group_by(self.Instance.project_id)
            .all()
        )

    def vm_total(self):
        # SELECT COUNT(*) FROM nova.instances WHERE NOT deleted;
        return (
            self.db.query(
                func.count(self.Instance.id))  # noqa: E126
            .filter(self.Instance.deleted == 0).scalar())

    def vm_created_deleted_last_hour(self):
        # vm_created_last_hour
        # SELECT * FROM nova.instances WHERE created_at BETWEEN <start_date>
        # AND <end_date>;
        utcnow = datetime.datetime.utcnow()
        start_date = (utcnow.replace(minute=0, second=0, microsecond=0)
                      - datetime.timedelta(hours=1))
        end_date = (utcnow.replace(minute=59, second=59, microsecond=0)
                    - datetime.timedelta(hours=1))

        created = (
            self.db.query(
                func.count(self.Instance.id))  # noqa: E126
            .filter(self.Instance.created_at.between(start_date, end_date))
            .scalar())

        # vm_deleted_last_hour
        # SELECT COUNT(*) FROM nova.instances WHERE deleted_at
        # BETWEEN <start_date> AND <end_date>;
        deleted = (
            self.db.query(
                func.count(self.Instance.id))  # noqa: E126
            .filter(self.Instance.deleted_at.between(start_date, end_date))
            .scalar())

        return created, deleted

    def get_undeleted_entries_shadow_tables(self):
        # SELECT COUNT(DELETED) FROM <table> WHERE NOT deleted;
        count = 0
        for t in self.db.get_db_tables():
            # Exclude tables 'shadow_migrate_version'
            # that do not have 'id' or 'deleted' attrs
            if 'shadow' in t and 'shadow_migrate_version' not in t:
                table = self.db.map_table(t)
                try:
                    count = count + (
                        self.db.query(
                            func.count(table.deleted))
                        .filter(table.deleted == 0).scalar()
                    )
                except Exception as ex:
                    logger.error(ex)
                    pass
        return count

    def hv_usage_per_aggregate_hosts(self, aggregate_hosts):
        hv_usage = {}

        # hv_cpu_total
        # hv_cpu_used
        # hv_ram_total
        # hv_ram_used
        # hv_disk_total
        # hv_disk_used
        # SELECT SUM(vcpus) AS vcpu, SUM(vcpus_used) AS vcpu_used,
        #        SUM(memory_mb) AS ram, SUM(memory_mb_used) AS ram_used,
        #        SUM(local_gb) AS disk, SUM(local_gb_used) AS disk_used
        # FROM nova.compute_nodes WHERE hypervisor_hostname IN
        #     (SELECT host FROM nova.services
        #      WHERE topic = "compute" AND NOT deleted
        #      AND host IN (aggregate_hosts));
        hv_tmp = (
            self.db.query(
                func.sum(self.ComputeNode.vcpus).label("hv_cpu_total"),  # noqa
                func.sum(self.ComputeNode.vcpus_used).label("hv_cpu_used"),
                func.sum(self.ComputeNode.memory_mb).label("hv_ram_total"),
                func.sum(self.ComputeNode.memory_mb_used).label("hv_ram_used"),
                func.sum(self.ComputeNode.local_gb).label("hv_disk_total"),
                func.sum(self.ComputeNode.local_gb_used).label("hv_disk_used"))
            .filter(
                self.Service.host.ilike(
                    self.ComputeNode.hypervisor_hostname.concat('%')))
            .filter(self.Service.topic == "compute")
            .filter(self.Service.host.in_(aggregate_hosts))
            .filter(self.Service.deleted == 0).one())

        hv_usage["hv_cpu_total"] = hv_tmp.hv_cpu_total or 0
        hv_usage["hv_cpu_used"] = hv_tmp.hv_cpu_used or 0
        hv_usage["hv_ram_total"] = hv_tmp.hv_ram_total or 0
        hv_usage["hv_ram_used"] = hv_tmp.hv_ram_used or 0
        hv_usage["hv_disk_total"] = hv_tmp.hv_disk_total or 0
        hv_usage["hv_disk_used"] = hv_tmp.hv_disk_used or 0

        return {k: int(v) for k, v in hv_usage.items()}
