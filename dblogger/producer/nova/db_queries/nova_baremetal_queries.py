import datetime
from sqlalchemy import func


class NovaBaremetalDBQueries:
    def __init__(self, db):
        """Initialize nova baremetal db queries."""
        self.db = db
        self.ComputeNode = self.db.map_table('compute_nodes')
        self.Service = self.db.map_table('services')
        self.Instance = self.db.map_table('instances')

    def vms_per_status(self):
        # SELECT COUNT(*), vm_state FROM nova.instances WHERE NOT deleted
        # GROUP BY vm_state;
        return (
            self.db.query(
                func.count(self.Instance.id),  # noqa: E126
                self.Instance.vm_state)
            .filter(self.Instance.deleted == 0)
            .group_by(self.Instance.vm_state).all())

    def vms_per_status_per_project(self):
        # SELECT COUNT(*), vm_state, project_id FROM nova.instances
        # WHERE NOT deleted
        # GROUP BY vm_state, project_id;
        return (
            self.db.query(
                func.count(self.Instance.id).label("count"),  # noqa: E126
                self.Instance.vm_state.label("status"),
                self.Instance.project_id)
            .filter(self.Instance.deleted == 0)
            .group_by(
                self.Instance.vm_state,
                self.Instance.project_id
            ).all()
        )

    def vm_usage_per_project_per_cell(self):
        # SELECT project_id, SUM(vcpus), SUM(memory_mb), COUNT(id) AS instances
        # FROM instances
        # WHERE NOT deleted
        # GROUP BY project_id;
        return (
            self.db.query(
                self.Instance.project_id,
                func.sum(self.Instance.vcpus).label("cores"),
                func.sum(self.Instance.memory_mb).label("ram"),
                func.count(self.Instance.id).label("instances"))
            .filter(self.Instance.deleted == 0)
            .group_by(self.Instance.project_id)
            .all()
        )

    def vm_total(self):
        # SELECT COUNT(*) FROM nova.instances WHERE NOT deleted;
        return (
            self.db.query(
                func.count(self.Instance.id))  # noqa: E126
            .filter(self.Instance.deleted == 0).scalar())

    def vm_created_deleted_last_hour(self):
        # vm_created_last_hour
        # SELECT * FROM nova.instances WHERE created_at BETWEEN <start_date>
        # AND <end_date>;
        utcnow = datetime.datetime.utcnow()
        start_date = (utcnow.replace(minute=0, second=0, microsecond=0)
                      - datetime.timedelta(hours=1))
        end_date = (utcnow.replace(minute=59, second=59, microsecond=0)
                    - datetime.timedelta(hours=1))

        created = (
            self.db.query(
                func.count(self.Instance.id))  # noqa: E126
            .filter(self.Instance.created_at.between(start_date, end_date))
            .scalar())

        # vm_deleted_last_hour
        # SELECT COUNT(*) FROM nova.instances WHERE deleted_at
        # BETWEEN <start_date> AND <end_date>;
        deleted = (
            self.db.query(
                func.count(self.Instance.id))  # noqa: E126
            .filter(self.Instance.deleted_at.between(start_date, end_date))
            .scalar())

        return created, deleted
