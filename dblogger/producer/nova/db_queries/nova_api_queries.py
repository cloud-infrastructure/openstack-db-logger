import logging
from sqlalchemy import func, or_
from sqlalchemy.orm import aliased

logger = logging.getLogger("dblogger")


class NovaAPIDBQueries:
    def __init__(self, db):
        """Initialize nova api db queries."""
        self.db = db
        self.Quotas = self.db.map_table('quotas')
        self.cell_mappings = self.db.map_table('cell_mappings')
        self.aggregates = self.db.map_table('aggregates')
        self.aggregate_metadata = self.db.map_table('aggregate_metadata')
        self.aggregate_hosts = self.db.map_table('aggregate_hosts')

    def limits_per_project(self):
        # SELECT SUM(hard_limit)
        #   FROM quotas
        #   WHERE resource = 'cores'
        #     OR resource = 'ram'
        #     OR resource = 'instances'
        #   GROUP BY project_id, resource
        return (
            self.db.query(
                func.sum(self.Quotas.hard_limit).label('hard_limit'),
                self.Quotas.resource,
                self.Quotas.project_id
            )
            .filter(
                or_(
                    self.Quotas.resource == 'cores',
                    self.Quotas.resource == 'ram',
                    self.Quotas.resource == 'instances'
                )
            )
            .group_by(
                self.Quotas.project_id,
                self.Quotas.resource
            )
            .all()
        )

    def get_cells_db_info(self):
        # SELECT database_connection, name
        #   FROM cell_mappings
        #   GROUP BY name, database_connection
        return (
            self.db.query(
                self.cell_mappings.name,
                self.cell_mappings.database_connection
            ).group_by(
                self.cell_mappings.name,
                self.cell_mappings.database_connection
            ).all()
        )

    def get_avz_by_cell_name(self, cell):
        # SELECT
        #     aggregate_metadata.value as cell,
        #     az.value as availability_zone
        # FROM
        #     aggregates
        # JOIN
        #     aggregate_metadata ON aggregates.id =
        #     aggregate_metadata.aggregate_id
        # LEFT JOIN
        #     aggregate_metadata AS az ON aggregates.id = az.aggregate_id
        # WHERE
        #     aggregate_metadata.key = 'cell'
        #     AND aggregate_metadata.value = 'gva_project_043'
        #    AND az.key = 'availability_zone';
        cell_metadata = aliased(self.aggregate_metadata)
        az_metadata = aliased(self.aggregate_metadata)
        query = (
            self.db.query(
                cell_metadata.value.label('cell'),
                az_metadata.value.label('availability_zone')
            ).join(
                self.aggregates,
                self.aggregates.id == cell_metadata.aggregate_id
            ).outerjoin(
                az_metadata,
                self.aggregates.id == az_metadata.aggregate_id
            ).filter(
                cell_metadata.key == 'cell',
                cell_metadata.value == cell,
                az_metadata.key == 'availability_zone'
            ).group_by(
                cell_metadata.value,
                az_metadata.value
            ).all()
        )
        result = {}
        if query:
            for row in query:
                result[cell] = row.availability_zone
        else:
            result[cell] = None
        return result

    def get_aggregates_info(self, cell):
        aggregates = (
            self.db.query(
                self.aggregates.name,
                self.aggregate_metadata.aggregate_id,
                self.aggregate_metadata.key,
                self.aggregate_metadata.value,
            )
            .join(
                self.aggregate_metadata,
                self.aggregates.id == self.aggregate_metadata.aggregate_id
            )
            .filter(
                self.aggregate_metadata.key == 'cell',
                self.aggregate_metadata.value == cell
            )
            .all()
        )
        cell_aggregates = {}
        for row in aggregates:
            data = (
                self.db.query(
                    self.aggregate_metadata.key,
                    self.aggregate_metadata.value
                )
                .filter(
                    self.aggregate_metadata.aggregate_id == row.aggregate_id
                )
                .all()
            )
            data_dict = {k: v for k, v in data}
            aggregate_info = {
                'aggregate_name': row.name,
                'arch': data_dict.get('arch'),
                'cell': data_dict.get('cell'),
                'cell_name': data_dict.get('cell_name'),
                'avz_zone': data_dict.get('availability_zone'),
                'delivery': data_dict.get('delivery'),
                'mode': data_dict.get('mode'),
                'netcluster': data_dict.get('netcluster'),
                'type': data_dict.get('type'),
                'install_date': data_dict.get('install_date', None),
                'enabled': data_dict.get('cell_type', 'disabled') == 'default',
            }
            cell_aggregates[row.name] = aggregate_info
        return cell_aggregates

    def get_hosts_per_aggregate(self, aggregate):
        aggregate_hosts = (
            self.db.query(
                self.aggregate_hosts.host,
                self.aggregates.name,
            )
            .join(
                self.aggregates,
                self.aggregate_hosts.aggregate_id == self.aggregates.id
            )
            .filter(
                self.aggregates.name == aggregate
            )
            .all()
        )
        hosts = []
        for row in aggregate_hosts:
            hosts.append(row.host)
        return hosts
