import json
import logging
import urllib.parse

from datetime import datetime
from datetime import timezone
from dateutil import parser

from dblogger.helpers.cache import NonExistentProject
from dblogger.producer.base import BaseDB
from dblogger.producer.nova.db_queries.nova_api_queries import \
    NovaAPIDBQueries
from dblogger.producer.nova.db_queries.nova_baremetal_queries import \
    NovaBaremetalDBQueries
from dblogger.producer.nova.db_queries.nova_cell_queries import \
    NovaCellDBQueries
from dblogger.storage.sql import SQL

logger = logging.getLogger("dblogger")


class Nova(BaseDB):
    def __init__(self, queue, section, configs, cache):
        """Initialize nova producer."""
        super(Nova, self).__init__(queue, section, configs, cache)
        self.region = self.configs.get(self.section, 'region')
        self.dblogger_user = self.configs.get(
            'dblogger', 'mysql_readonly_user')
        self.dblogger_password = self.configs.get(
            'dblogger', 'mysql_readonly_password')
        self.ignored_cells = self.configs.get(
            'dblogger', 'nova_producer_ignored_cells')
        self.ignored_cells = json.loads(self.ignored_cells)
        self.nova_api_queries = NovaAPIDBQueries(self.db)
        self.baremetal_pattern = self.configs.get(
            'dblogger', 'baremetal_cells_pattern')

    def _run_queries(self):
        self._run_nova_api_queries()
        cells_info = self._get_cells_db()
        for cell_name, cell_connection in cells_info.items():
            if (self.baremetal_pattern in cell_name
                    and cell_name not in self.ignored_cells):
                db = SQL(cell_connection)
                self.nova_baremetal_queries = NovaBaremetalDBQueries(db)
                aggregate_info = self.nova_api_queries.get_aggregates_info(
                    cell_name)
                self._run_nova_baremetal_queries(cell_name, aggregate_info)
                db.close()
            elif (self.baremetal_pattern not in cell_name
                  and cell_name not in self.ignored_cells):
                db = SQL(cell_connection)
                self.nova_cell_queries = NovaCellDBQueries(db)
                self.avzs = self.nova_api_queries.get_avz_by_cell_name(
                    cell_name)
                aggregate_info = self.nova_api_queries.get_aggregates_info(
                    cell_name)
                self._run_nova_cell_queries(cell_name, aggregate_info)
                db.close()
            else:
                logger.debug("Not generating metrics for cell %s" % cell_name)

    def _get_cells_db(self):
        try:
            cells_info = {}
            for row in self.nova_api_queries.get_cells_db_info():
                url = urllib.parse.urlparse(row.database_connection)
                host_port = url.netloc.split("@")[1]
                new_c = url._replace(netloc="{}:{}@{}".format(
                    self.dblogger_user,
                    self.dblogger_password,
                    host_port)).geturl()
                cells_info[row.name] = new_c
            return cells_info
        except Exception as ex:
            logger.exception("Error getting database connection: %s" % ex)

    def _run_nova_api_queries(self):
        logger.debug("run nova_api for region %s" % self.region)
        limits = self.nova_api_queries.limits_per_project()

        for row in limits:
            quota_limit = row.hard_limit or 0
            try:
                project_metadata = self.cache.get_project_metadata(
                    row.project_id)
            except NonExistentProject:
                continue  # Ignore, the project doesn't exist

            if project_metadata['chargegroup'] != 'unknown':
                self.produce_metric(
                    metric='{0}_limits_per_project'.format(row.resource),
                    headers={
                        'region': self.region,
                        'project_id': row.project_id,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                    },
                    values={
                        'quota': int(quota_limit)
                    }
                )

                # OS-6657 -> hack to make the join success with project
                # with no usage
                self.produce_metric(
                    metric='{0}_usage_per_project_per_cell'.format(
                        row.resource),
                    headers={
                        'region': self.region,
                        'cell': 'fake',
                        'project_id': row.project_id,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                    },
                    values={
                        'usage': int(0)
                    }
                )
        logger.debug("end run nova_api for region %s" % self.region)

    def _run_nova_baremetal_queries(self, cell, aggregate_info):
        logger.debug("run nova_baremetal for cell %s" % cell)
        self.kind = 'physical'

        self.produce_metric(
            metric='vms_per_cell',
            headers={
                'cell': cell,
                'region': self.region,
                'type': self.kind,
            },
            values={
                'count': int(self.nova_baremetal_queries.vm_total())
            })

        created, deleted = (
            self.nova_baremetal_queries.vm_created_deleted_last_hour()
        )

        self.produce_metric(
            metric='vms_changes_per_cell',
            headers={
                'cell': cell,
                'region': self.region,
                'type': self.kind,
            },
            values={
                'created_in_last_hour': int(created),
                'deleted_in_last_hour': int(deleted),
            })

        for count, status in self.nova_baremetal_queries.vms_per_status():
            self.produce_metric(
                metric='vms_per_status_per_cell',
                headers={
                    'cell': cell,
                    'region': self.region,
                    'status': status,
                    'type': self.kind,
                },
                values={
                    'count': int(count)
                }
            )

        for row in self.nova_baremetal_queries.vms_per_status_per_project():
            try:
                project_metadata = self.cache.get_project_metadata(
                    row.project_id)
            except NonExistentProject:
                continue  # Ignore, the project doesn't exist

            if project_metadata['chargegroup'] != 'unknown':
                self.produce_metric(
                    metric='vms_per_status_per_cell_per_project',
                    headers={
                        'cell': cell,
                        'region': self.region,
                        'status': row.status,
                        'project_id': row.project_id,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                        'type': self.kind,
                    },
                    values={
                        'count': int(row.count)
                    }
                )

        for row in self.nova_baremetal_queries.vm_usage_per_project_per_cell():
            try:
                project_metadata = self.cache.get_project_metadata(
                    row.project_id)
            except NonExistentProject:
                continue  # Ignore, the project doesn't exist

            if project_metadata['chargegroup'] != 'unknown':
                self.produce_metric(
                    metric='instances_usage_per_project_per_cell',
                    headers={
                        'cell': cell,
                        'region': self.region,
                        'project_id': row.project_id,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                        'type': self.kind,
                    },
                    values={
                        'usage': int(row.instances),
                    }
                )

                self.produce_metric(
                    metric='ram_usage_per_project_per_cell',
                    headers={
                        'cell': cell,
                        'region': self.region,
                        'project_id': row.project_id,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                        'type': self.kind,
                    },
                    values={
                        'usage': int(row.ram),
                    }
                )

                self.produce_metric(
                    metric='cores_usage_per_project_per_cell',
                    headers={
                        'cell': cell,
                        'region': self.region,
                        'project_id': row.project_id,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                        'type': self.kind,
                    },
                    values={
                        'usage': int(row.cores),
                    }
                )
        logger.debug("end run nova_baremetal for cell %s" % cell)

    def _run_nova_cell_queries(self, cell, aggregate_info):
        logger.debug("run nova_cell for cell %s" % cell)
        self.kind = 'virtual'
        avz_zone = self.avzs[cell]
        if avz_zone is None:
            avz_zone = 'nova'

        utcnow = datetime.now(timezone.utc)
        for aggregate in aggregate_info:
            aggregate = aggregate_info[aggregate]

            if aggregate['install_date']:
                install_date = parser.parse(
                    aggregate['install_date']).replace(tzinfo=timezone.utc)
            else:
                logger.warning("No install_date for aggregate %s",
                               aggregate['aggregate_name'])
                install_date = utcnow
            age = int((utcnow - install_date).total_seconds())

            hosts = self.nova_api_queries.get_hosts_per_aggregate(
                aggregate['aggregate_name'])
            usage = self.nova_cell_queries.hv_usage_per_aggregate_hosts(hosts)

            self.produce_metric(
                metric='hypervisors_ram_per_aggregate',
                headers={
                    'aggregate': aggregate['aggregate_name'],
                    'aggregate_type': aggregate['type'],
                    'arch': aggregate['arch'],
                    'avz_zone': avz_zone,
                    'cell': aggregate['cell_name'],
                    'delivery': aggregate['delivery'],
                    'mode': aggregate['mode'],
                    'netcluster': aggregate['netcluster'],
                    'region': self.region,
                    'enabled': aggregate['enabled'],
                },
                values={
                    'total': int(usage['hv_ram_total']),
                    'used': int(usage['hv_ram_used']),
                    'age': age,
                })
            self.produce_metric(
                metric='hypervisors_cpu_per_aggregate',
                headers={
                    'aggregate': aggregate['aggregate_name'],
                    'aggregate_type': aggregate['type'],
                    'arch': aggregate['arch'],
                    'avz_zone': avz_zone,
                    'cell': aggregate['cell_name'],
                    'delivery': aggregate['delivery'],
                    'mode': aggregate['mode'],
                    'netcluster': aggregate['netcluster'],
                    'region': self.region,
                    'enabled': aggregate['enabled'],
                },
                values={
                    'total': int(usage['hv_cpu_total']),
                    'used': int(usage['hv_cpu_used']),
                    'age': age,
                })
            self.produce_metric(
                metric='hypervisors_disk_per_aggregate',
                headers={
                    'aggregate': aggregate['aggregate_name'],
                    'aggregate_type': aggregate['type'],
                    'arch': aggregate['arch'],
                    'avz_zone': avz_zone,
                    'cell': aggregate['cell_name'],
                    'delivery': aggregate['delivery'],
                    'mode': aggregate['mode'],
                    'netcluster': aggregate['netcluster'],
                    'region': self.region,
                    'enabled': aggregate['enabled'],
                },
                values={
                    'total': int(usage['hv_disk_total']),
                    'used': int(usage['hv_disk_used']),
                    'age': age,
                })

        self.produce_metric(
            metric="undeleted_entries_shadow_tables",
            headers={
                'cell': cell
            },
            values={
                'count': int(
                    self.nova_cell_queries.
                    get_undeleted_entries_shadow_tables()
                )
            }
        )
        self.produce_metric(
            metric='hypervisors_per_cell',
            headers={
                'cell': cell,
                'region': self.region,
            },
            values={
                'disabled': int(self.nova_cell_queries.hv_disabled()),
                'total': int(self.nova_cell_queries.hv_total())
            })

        for (count, memory, processor, model) in self.nova_cell_queries.\
                hv_total_per_processor():
            processor = processor.replace('"', '')
            model = model.replace('"', '')
            self.produce_metric(
                metric='hypervisors_per_processor_per_cell',
                headers={
                    'cell': cell,
                    'region': self.region,
                    'processor': processor,
                    'model': model,
                },
                values={
                    'count': int(count),
                    'memory': int(memory)
                }
            )

        usage = self.nova_cell_queries.hv_usage()
        self.produce_metric(
            metric='hypervisors_ram_per_cell',
            headers={
                'cell': cell,
                'region': self.region,
            },
            values={
                'total': int(usage['hv_ram_total']),
                'used': int(usage['hv_ram_used']),
            })
        self.produce_metric(
            metric='hypervisors_cpu_per_cell',
            headers={
                'cell': cell,
                'region': self.region,
            },
            values={
                'total': int(usage['hv_cpu_total']),
                'used': int(usage['hv_cpu_used']),
            })
        self.produce_metric(
            metric='hypervisors_disk_per_cell',
            headers={
                'cell': cell,
                'region': self.region,
            },
            values={
                'total': int(usage['hv_disk_total']),
                'used': int(usage['hv_disk_used']),
            })

        for (hostname, hv_cores, vm_cores, hv_ram,
             vm_ram, hv_disk, vm_disk,
             vm_count, hv_version,
             hv_disabled) in self.nova_cell_queries.hv_detail():
            self.produce_metric(
                metric='hypervisor_detail',
                headers={
                    'cell': cell,
                    'region': self.region,
                    'avz_zone': avz_zone,
                    'hostname': hostname,
                    'hv_version': int(hv_version),
                    'hv_disabled': int(hv_disabled)
                },
                values={
                    'hv_cores': int(hv_cores),
                    'vm_cores': int(vm_cores),
                    'hv_ram': int(hv_ram),
                    'vm_ram': int(vm_ram),
                    'hv_disk': int(hv_disk),
                    'vm_disk': int(vm_disk),
                    'vm_count': int(vm_count)
                }
            )

        self.produce_metric(
            metric='vms_per_cell',
            headers={
                'cell': cell,
                'region': self.region,
                'type': self.kind,
            },
            values={
                'count': int(self.nova_cell_queries.vm_total())
            })

        created, deleted = (
            self.nova_cell_queries.vm_created_deleted_last_hour())

        self.produce_metric(
            metric='vms_changes_per_cell',
            headers={
                'cell': cell,
                'region': self.region,
                'type': self.kind,
            },
            values={
                'created_in_last_hour': int(created),
                'deleted_in_last_hour': int(deleted),
            })

        for count, status in self.nova_cell_queries.vms_per_status():
            self.produce_metric(
                metric='vms_per_status_per_cell',
                headers={
                    'cell': cell,
                    'region': self.region,
                    'status': status,
                    'type': self.kind,
                },
                values={
                    'count': int(count)
                }
            )

        for row in self.nova_cell_queries.vms_per_status_per_project():
            try:
                project_metadata = self.cache.get_project_metadata(
                    row.project_id)
            except NonExistentProject:
                continue  # Ignore, the project doesn't exist

            if project_metadata['chargegroup'] != 'unknown':
                self.produce_metric(
                    metric='vms_per_status_per_cell_per_project',
                    headers={
                        'cell': cell,
                        'region': self.region,
                        'status': row.status,
                        'project_id': row.project_id,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                        'type': self.kind,
                    },
                    values={
                        'count': int(row.count)
                    }
                )

        for row in self.nova_cell_queries.vm_usage_per_project_per_cell():
            try:
                project_metadata = self.cache.get_project_metadata(
                    row.project_id)
            except NonExistentProject:
                continue  # Ignore, the project doesn't exist

            if project_metadata['chargegroup'] != 'unknown':
                self.produce_metric(
                    metric='instances_usage_per_project_per_cell',
                    headers={
                        'cell': cell,
                        'region': self.region,
                        'project_id': row.project_id,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                        'type': self.kind,
                    },
                    values={
                        'usage': int(row.instances),
                    }
                )

                self.produce_metric(
                    metric='ram_usage_per_project_per_cell',
                    headers={
                        'cell': cell,
                        'region': self.region,
                        'project_id': row.project_id,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                        'type': self.kind,
                    },
                    values={
                        'usage': int(row.ram),
                    }
                )

                self.produce_metric(
                    metric='cores_usage_per_project_per_cell',
                    headers={
                        'cell': cell,
                        'region': self.region,
                        'project_id': row.project_id,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                        'type': self.kind,
                    },
                    values={
                        'usage': int(row.cores),
                    }
                )
        logger.debug("end run nova_cell for cell %s" % cell)
