import abc
from dblogger.storage.sql import SQL
from keystoneauth1 import session as keystone_session
from os_client_config import config as cloud_config


class Base(object):

    def __init__(self, queue, section, configs, cache):
        """Initialize base producer."""
        super(Base, self).__init__()
        self.queue = queue
        self.configs = configs
        self.section = section
        self.cache = cache
        self.region = self.configs.get(self.section, 'region')

    def run(self):
        self._run_queries()

    @abc.abstractmethod
    def _run_queries(self):
        """Extract info from OS databases."""
        pass

    def produce_metric(self, metric, headers=None, values=None):
        """Produce a metric that can be understood by consumers.

        :args metric: Metric name
        :args headers: A dict with metadata for the metric
        :args values: A dict with actual values for the metric
        :returns: A dict with `header` and `values`
        """
        metric = {
            "header": {
                "metric": metric
            },
            "values": {
            }
        }

        if headers is not None:
            metric['header'].update(headers)
        if values is not None:
            metric['values'].update(values)

        self.queue.put_nowait(metric)


class BaseDB(Base):

    def __init__(self, queue, section, configs, cache):
        """Initialize base db producer."""
        super(BaseDB, self).__init__(queue, section, configs, cache)
        # Create DB connection
        self.db = SQL(self.configs.get(self.section, 'connection'))

    def run(self):
        super(BaseDB, self).run()
        self.db.close()


class BaseCloud(Base):

    def __init__(self, queue, section, configs, cache):
        """Initialize base cloud producer."""
        super(BaseCloud, self).__init__(queue, section, configs, cache)

    def get_session(self, cloud):
        # Create session based on the arguments passed by parameter
        try:
            cloud_cfg = cloud_config.OpenStackConfig()
        except (IOError, OSError) as e:
            raise e

        cloud_obj = cloud_cfg.get_one_cloud(cloud=cloud)
        return keystone_session.Session(auth=cloud_obj.get_auth())

    def get_endpoints_per_service(self,
                                  service_type,
                                  session,
                                  iface='public',
                                  region=None):

        # Retrieve the catalog from the session
        cat_obj = session.auth.get_auth_ref(session).service_catalog.catalog

        catalog = cat_obj['catalog'] if 'catalog' in cat_obj else cat_obj

        # Get the endpoints for a service type
        if catalog:
            endpoints = [entry['endpoints'] for entry in catalog
                         if entry['type'] == service_type].pop()
        else:
            endpoints = []

        if iface:
            endpoints = [ep for ep in endpoints if ep['interface'] == iface]
        if region:
            endpoints = [ep for ep in endpoints if ep['region'] == region]
        return endpoints
