import logging

from dblogger.producer.base import BaseDB
from sqlalchemy import and_
from sqlalchemy import func

logger = logging.getLogger("dblogger")


class Glance(BaseDB):

    def __init__(self, queue, section, configs, cache):
        """Initialize glance producer."""
        super(Glance, self).__init__(queue, section, configs, cache)
        # Create table mapping
        self.Images = self.db.map_table('images')

    def _run_queries(self):
        logger.debug('run glance')
        # Generate glance generic data
        self.produce_metric(
            metric="images_total",
            headers={
                'region': self.region
            },
            values={
                "all": int(self.image_num()),
                "active": int(self.image_active_num()),
                "size": float(self.image_size()),
            })

        for row in self.images_per_status():
            self.produce_metric(
                metric='images_per_status',
                headers={
                    'region': self.region,
                    'status': row.status
                },
                values={
                    'count': int(row.count)
                }
            )

        for row in self.images_per_type():
            self.produce_metric(
                metric='images_per_type',
                headers={
                    'region': self.region,
                    'type': row.disk_format
                },
                values={
                    'count': int(row.count)
                }
            )
        logger.debug('end run glance')

    def image_num(self):
        # SELECT COUNT(id) FROM images;
        return (self.db.query(
            func.count(self.Images.id).label("count")).
            scalar())

    def image_active_num(self):
        # SELECT COUNT(id) FROM images WHERE status='active';
        return (self.db.query(
            func.count(self.Images.id).label("count")).
            filter(self.Images.status == 'active').scalar())

    def image_size(self):
        # SELECT SUM(size) FROM images WHERE status='active';
        return (self.db.query(
            func.sum(self.Images.size).label("size")).
            filter(self.Images.status == 'active').scalar())

    def images_per_status(self):
        # SELECT status, count(status)
        #   FROM images
        #   WHERE deleted = 0
        #   GROUP BY status;
        return (
            self.db.query(
                func.count(self.Images.status).label("count"),
                self.Images.status.label("status"))
            .filter(self.Images.deleted == 0)
            .group_by(self.Images.status)
            .all()
        )

    def images_per_type(self):
        # SELECT disk_format, count(disk_format)
        #   FROM IMAGES
        #   WHERE deleted = 0
        #     AND disk_format IS NOT NULL
        #   GROUP BY disk_format
        return (
            self.db.query(
                func.count(self.Images.disk_format).label("count"),
                self.Images.disk_format.label("disk_format"))
            .filter(
                and_(self.Images.deleted == 0,
                    self.Images.disk_format != None))  # noqa
            .group_by(self.Images.disk_format)
            .all()
        )
