import logging

from dblogger.helpers.cache import NonExistentProject
from dblogger.producer.base import BaseDB
from sqlalchemy import and_
from sqlalchemy import func

logger = logging.getLogger("dblogger")


class Cinder(BaseDB):

    def __init__(self, queue, section, configs, cache):
        """Initialize cinder producer."""
        super(Cinder, self).__init__(queue, section, configs, cache)
        # Create table mapping
        self.Quota = self.db.map_table('quotas')
        self.QuotaUsage = self.db.map_table('quota_usages')
        self.Volumes = self.db.map_table('volumes')
        self.VolumeTypes = self.db.map_table('volume_types')
        self.Backups = self.db.map_table('backups')

    def _run_queries(self):
        logger.debug('run cinder')
        # Generate cinder totals
        count, size = self.volume_total_data()

        self.produce_metric(
            metric="volumes_total",
            headers={
                'region': self.region,
            },
            values={
                "count": int(count),
                "size": float(size)
            }
        )

        # Generate cinder totals per type
        for v in self.volume_data():
            self.produce_metric(
                metric="volumes_per_type",
                headers={
                    'region': self.region,
                    "type": v.name
                },
                values={
                    "count": int(v.count),
                    "size": float(v.size),
                }
            )

        for row in self.volume_total_per_status():
            self.produce_metric(
                metric="volumes_per_status",
                headers={
                    'region': self.region,
                    "status": row.status
                },
                values={
                    "count": int(row.count)
                }
            )

        count, size = self.backup_total_data()
        self.produce_metric(
            metric="backups_total",
            headers={
                'region': self.region,
            },
            values={
                "count": int(count),
                "size": float(size or 0.0)
            }
        )

        for row in self.backup_total_per_status():
            self.produce_metric(
                metric="backups_per_status",
                headers={
                    'region': self.region,
                    "status": row.status
                },
                values={
                    "count": int(row.count)
                }
            )

        # It needs some massaging to split it in different tables
        for row in self.quotas():
            quota_limit = row.quota_limit or 0
            quota_usage = row.quota_usage or 0
            try:
                project_metadata = self.cache.get_project_metadata(
                    row.project_id)
            except NonExistentProject:
                continue  # Ignore, the project doesn't exist

            if project_metadata['chargegroup'] != 'unknown':
                if row.resource in ('volumes', 'snapshots', 'gigabytes',
                                    'backups', 'backup_gigabytes'):
                    if row.resource == 'volumes':
                        metric_name = 'volumes'
                    elif row.resource == 'gigabytes':
                        metric_name = 'volumes_gb'
                    elif row.resource == 'snapshots':
                        metric_name = 'snapshots'
                    elif row.resource == 'backups':
                        metric_name = 'backups'
                    elif row.resource == 'backup_gigabytes':
                        metric_name = 'backup_gigabytes'

                    self.produce_metric(
                        metric="{0}_quota_per_project".format(metric_name),
                        headers={
                            'project_id': row.project_id,
                            'region': self.region,
                            'project_type': project_metadata['project_type'],
                            'project_name': project_metadata['project_name'],
                            'chargerole': project_metadata['chargerole'],
                            'chargegroup': project_metadata['chargegroup'],
                            'chargegroup_name':
                                project_metadata['chargegroup_data']['name'],
                        },
                        values={
                            "quota": int(quota_limit),
                            "usage": int(quota_usage),
                        }
                    )
                elif row.resource.startswith('volumes_'):
                    # We get the volume type from names like volumes_cpio1
                    volume_type = row.resource.split('_')[1]
                    self.produce_metric(
                        metric='volumes_quota_per_type_per_project',
                        headers={
                            'type': volume_type,
                            'project_id': row.project_id,
                            'region': self.region,
                            'project_type': project_metadata['project_type'],
                            'project_name': project_metadata['project_name'],
                            'chargerole': project_metadata['chargerole'],
                            'chargegroup': project_metadata['chargegroup'],
                            'chargegroup_name':
                                project_metadata['chargegroup_data']['name'],
                        },
                        values={
                            "quota": int(quota_limit),
                            "usage": int(quota_usage),
                        })
                elif row.resource.startswith('gigabytes_'):
                    # We get the volume type from names like volumes_cpio1
                    volume_type = row.resource.split('_')[1]
                    self.produce_metric(
                        metric='volumes_gb_quota_per_type_per_project',
                        headers={
                            'type': volume_type,
                            'project_id': row.project_id,
                            'region': self.region,
                            'project_type': project_metadata['project_type'],
                            'project_name': project_metadata['project_name'],
                            'chargerole': project_metadata['chargerole'],
                            'chargegroup': project_metadata['chargegroup'],
                            'chargegroup_name':
                                project_metadata['chargegroup_data']['name'],
                        },
                        values={
                            "quota": int(quota_limit),
                            "usage": int(quota_usage),
                        })
                else:
                    continue  # Ignore snapshot types and backups
        logger.debug('end run cinder')

    def volume_data(self):
        # SELECT volume_types.name, COUNT(*), SUM(volumes.size)
        # FROM volumes
        # INNER JOIN volume_types
        #   ON volumes.volume_type_id = volume_types.id
        # WHERE volumes.deleted = 0
        # GROUP BY volumes.volume_type_id;
        tmp_data = (
            self.db.query(
                func.count(self.Volumes.id).label("count"),  # noqa: E126
                func.sum(self.Volumes.size).label("size"),
                self.VolumeTypes.name.label("name"))
            .outerjoin(self.VolumeTypes,
                       self.Volumes.volume_type_id == self.VolumeTypes.id)
            .filter(self.Volumes.deleted == 0)
            .group_by(self.Volumes.volume_type_id).all())

        return tmp_data

    def volume_total_per_status(self):
        # SELECT volume.status, COUNT(*)
        # FROM volumes
        # WHERE volumes.deleted = 0
        # GROUP BY volume.status;
        return (
            self.db.query(
                func.count(self.Volumes.id).label("count"),  # noqa: E126
                self.Volumes.status.label("status"))
            .filter(self.Volumes.deleted == 0)
            .group_by(self.Volumes.status).all())

    def volume_total_data(self):
        # SELECT COUNT(*), SUM(volumes.size)
        # FROM volumes
        # WHERE volumes.deleted = 0
        return (
            self.db.query(
                func.count(self.Volumes.id).label("count"),  # noqa: E126
                func.sum(self.Volumes.size).label("size"))
            .filter(self.Volumes.deleted == 0)
            .one())

    def backup_total_per_status(self):
        # SELECT backups.status, COUNT(*)
        # FROM backups
        # WHERE backups.deleted = 0
        # GROUP BY backups.status;
        return (
            self.db.query(
                func.count(self.Backups.id).label("count"),  # noqa: E126
                self.Backups.status.label("status"))
            .filter(self.Backups.deleted == 0)
            .group_by(self.Backups.status).all())

    def backup_total_data(self):
        # SELECT COUNT(*), SUM(backups.size)
        # FROM backups
        # WHERE backups.deleted = 0
        return (
            self.db.query(
                func.count(self.Backups.id).label("count"),  # noqa: E126
                func.sum(self.Backups.size).label("size"))
            .filter(self.Backups.deleted == 0)
            .one())

    def quotas(self):
        # SELECT quotas.project_id, quotas.resource, quotas.hard_limit,
        #        usages.in_use
        # FROM cinder.quotas AS quotas
        # INNER JOIN cinder.quota_usages AS usages
        #   ON quotas.project_id = usages.project_id
        #     AND quotas.resource = usages.resource
        # WHERE quotas.hard_limit != 0
        #   AND quotas.deleted = 0
        # ORDER BY quotas.project_id;
        tmp_data = (
            self.db.query(
                self.Quota.project_id.label("project_id"),  # noqa: E128
                self.Quota.resource.label("resource"),
                self.Quota.hard_limit.label("quota_limit"),
                self.QuotaUsage.in_use.label("quota_usage"))
            .outerjoin(self.QuotaUsage, and_(
                self.Quota.project_id == self.QuotaUsage.project_id,
                self.Quota.resource == self.QuotaUsage.resource))
            .filter(self.Quota.hard_limit > 0)
            .filter(self.Quota.deleted == 0)
            .order_by(self.Quota.project_id).all())

        return tmp_data
