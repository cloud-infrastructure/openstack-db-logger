import logging

from dblogger.helpers.cache import NonExistentProject
from dblogger.producer.base import BaseDB
from sqlalchemy import and_
from sqlalchemy import func

logger = logging.getLogger("dblogger")


class Manila(BaseDB):

    def __init__(self, queue, section, configs, cache):
        """Initialize manila producer."""
        super(Manila, self).__init__(queue, section, configs, cache)

        # Create table mapping
        self.Quota = self.db.map_table('quotas')
        self.ProjectShareTypeQuota = self.db.map_table(
            'project_share_type_quotas')
        self.QuotaUsage = self.db.map_table('quota_usages')
        self.Shares = self.db.map_table('shares')
        self.ShareInstances = self.db.map_table('share_instances')
        self.ShareTypes = self.db.map_table('share_types')

    def _run_queries(self):
        logger.debug('run manila')
        shares = self.shares()
        self.produce_metric(
            metric="fileshares_total",
            headers={
                'region': self.region
            },
            values={
                "count": int(shares.count)
            }
        )

        self.produce_metric(
            metric="fileshares_gb_total",
            headers={
                'region': self.region
            },
            values={
                "usage": float(shares.size) if shares and shares.size else 0.0
            }
        )

        for row in self.shares_per_status():
            self.produce_metric(
                metric="fileshares_per_status",
                headers={
                    'region': self.region,
                    "status": row.status
                },
                values={
                    "count": int(row.count)
                }
            )

        # get quotas per project
        for row in self.quotas():
            try:
                project_metadata = self.cache.get_project_metadata(
                    row.project_id)
            except NonExistentProject:
                continue  # Ignore, the project doesn't exist

            if project_metadata['chargegroup'] != 'unknown':
                if row.resource == "gigabytes":
                    DEFAULT_GB_QUOTA_LIMIT = 1000
                    metric_name = "fileshares_gb_quota_per_project"
                    quota_limit = row.quota_limit or DEFAULT_GB_QUOTA_LIMIT
                elif row.resource == "shares":
                    DEFAULT_SHARES_QUOTA_LIMIT = 50
                    metric_name = "fileshares_quota_per_project"
                    quota_limit = row.quota_limit or DEFAULT_SHARES_QUOTA_LIMIT
                else:
                    continue  # Ignore, only interested in count+size

                quota_usage = row.quota_usage or 0  # default

                self.produce_metric(
                    metric=metric_name,
                    headers={
                        'project_id': row.project_id,
                        'region': self.region,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                    },
                    values={
                        "quota": int(quota_limit),
                        "usage": int(quota_usage),
                    }
                )

        # get quotas per project and per type
        for row in self.quotas_per_type():
            try:
                project_metadata = self.cache.get_project_metadata(
                    row.project_id)
            except NonExistentProject:
                continue  # Ignore, the project doesn't exist

            if project_metadata['chargegroup'] != 'unknown':
                if row.resource == "gigabytes":
                    DEFAULT_GB_QUOTA_LIMIT = 1000
                    metric_name = "fileshares_gb_quota_per_type_per_project"
                    quota_limit = row.quota_limit or DEFAULT_GB_QUOTA_LIMIT
                elif row.resource == "shares":
                    DEFAULT_SHARES_QUOTA_LIMIT = 50
                    metric_name = "fileshares_quota_per_type_per_project"
                    quota_limit = row.quota_limit or DEFAULT_SHARES_QUOTA_LIMIT
                else:
                    continue  # Ignore, only interested in count+size

                quota_usage = row.quota_usage or 0  # default

                self.produce_metric(
                    metric=metric_name,
                    headers={
                        'project_id': row.project_id,
                        'type': row.share_type,
                        'region': self.region,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                    },
                    values={
                        "quota": int(quota_limit),
                        "usage": int(quota_usage),
                    }
                )

        for row in self.shares_per_project():
            try:
                project_metadata = self.cache.get_project_metadata(
                    row.project_id)
            except NonExistentProject:
                continue  # Ignore, the project doesn't exist

            if project_metadata['chargegroup'] != 'unknown':
                self.produce_metric(
                    metric="fileshares_per_project",
                    headers={
                        'project_id': row.project_id,
                        'region': self.region,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                    },
                    values={
                        "count": int(row.count)
                    }
                )

                self.produce_metric(
                    metric="fileshares_gb_per_project",
                    headers={
                        'project_id': row.project_id,
                        'region': self.region,
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                    },
                    values={
                        "usage": float(row.size)
                    }
                )
        logger.debug('end run manila')

    def shares(self):
        # SELECT COUNT(shares.id), SUM(shares.size)
        #   FROM share_instances
        #   INNER JOIN shares
        #     ON share_instances.share_id = shares.id
        #   WHERE share_instances.deleted = "False"
        return (
            self.db.query(
                func.count(self.Shares.id).label("count"),
                func.sum(self.Shares.size).label("size")
            )
            .join(
                self.ShareInstances,
                self.ShareInstances.share_id == self.Shares.id
            )
            .filter(
                self.ShareInstances.deleted == 'False'
            )
            .one()
        )

    def shares_per_status(self):
        # SELECT i.status, COUNT(s.id)
        #   FROM share_instances i
        #   INNER JOIN shares s ON i.share_id = s.id
        #   WHERE i.deleted = "False"
        #   GROUP BY i.`status`;
        return (
            self.db.query(
                func.count(self.Shares.id).label("count"),  # noqa: E126
                self.ShareInstances.status.label("status"))
            .join(
                self.ShareInstances,
                self.ShareInstances.share_id == self.Shares.id
            )
            .filter(
                self.ShareInstances.deleted == 'False'
            )
            .group_by(self.ShareInstances.status).all())

    def shares_per_project(self):
        # SELECT COUNT(shares.id), SUM(shares.size), shares.project_id
        #    FROM share_instances
        #    INNER JOIN shares
        #      ON share_instances.share_id = shares.id
        #    WHERE share_instances.deleted = "False"
        #    GROUP BY shares.project_id
        return (
            self.db.query(
                func.count(self.Shares.id).label("count"),
                func.sum(self.Shares.size).label("size"),
                self.Shares.project_id
            )
            .join(
                self.ShareInstances,
                self.ShareInstances.share_id == self.Shares.id
            )
            .filter(
                self.ShareInstances.deleted == "False"
            )
            .group_by(
                self.Shares.project_id
            )
        )

    def quotas(self):
        # SELECT
        #   quota_usages.project_id,
        #   quota_usages.resource,
        #   sum(in_use),
        #   max(quotas.hard_limit)
        # FROM quota_usages
        # LEFT JOIN quotas
        #   ON quotas.project_id = quota_usages.project_id
        #   AND quotas.resource = quota_usages.resource
        # WHERE share_type_id IS NOT NULL
        # GROUP BY resource, project_id
        # ORDER BY project_id, resource;
        return (
            self.db.query(
                self.QuotaUsage.project_id.label("project_id"),
                self.QuotaUsage.resource.label("resource"),
                func.sum(self.QuotaUsage.in_use).label("quota_usage"),
                func.max(self.Quota.hard_limit).label("quota_limit")
            )
            .join(
                self.Quota,
                and_(
                    self.Quota.project_id == self.QuotaUsage.project_id,
                    self.Quota.resource == self.QuotaUsage.resource
                ),
                isouter=True
            )
            .filter(
                self.QuotaUsage.share_type_id.isnot(None)
            )
            .group_by(
                self.QuotaUsage.resource,
                self.QuotaUsage.project_id
            )
        )

    def quotas_per_type(self):
        # SELECT
        #   quota_usages.project_id,
        #   quota_usages.resource,
        #   share_types.name AS share_type,
        #   sum(in_use) AS quota_usage,
        #   max(hard_limit) AS quota_limit
        # FROM quota_usages
        # LEFT JOIN project_share_type_quotas
        #   ON project_share_type_quotas.project_id = quota_usages.project_id
        #   AND project_share_type_quotas.resource = quota_usages.resource
        #   AND project_share_type_quotas.share_type_id =
        #           quota_usages.share_type_id
        # LEFT JOIN share_types
        #   ON share_types.id = quota_usages.share_type_id
        # WHERE quota_usages.share_type_id IS NOT NULL
        # GROUP BY resource, project_id, quota_usages.share_type_id
        # ORDER BY project_id, name, RESOURCE;
        return (
            self.db.query(
                self.QuotaUsage.project_id.label("project_id"),
                self.QuotaUsage.resource.label("resource"),
                self.ShareTypes.name.label("share_type"),
                func.sum(self.QuotaUsage.in_use).label("quota_usage"),
                func.max(self.ProjectShareTypeQuota.hard_limit).label(
                    "quota_limit")
            )
            .join(
                self.ProjectShareTypeQuota,
                and_(
                    self.ProjectShareTypeQuota.project_id
                    == self.QuotaUsage.project_id,
                    self.ProjectShareTypeQuota.resource
                    == self.QuotaUsage.resource,
                    self.ProjectShareTypeQuota.share_type_id
                    == self.QuotaUsage.share_type_id
                ),
                isouter=True
            )
            .join(
                self.ShareTypes,
                self.ShareTypes.id == self.QuotaUsage.share_type_id
            )
            .filter(
                self.QuotaUsage.share_type_id.isnot(None)
            )
            .group_by(
                self.QuotaUsage.resource,
                self.QuotaUsage.project_id,
                self.QuotaUsage.share_type_id
            )
        )
