import json
import logging
import requests

from dblogger.producer.base import Base

logger = logging.getLogger("dblogger")


class ESPuppetDB(Base):
    def __init__(self, queue, section, configs, cache):
        """Initialize puppetdb producer."""
        super(ESPuppetDB, self).__init__(queue, section, configs, cache)
        self.authorization = self.configs.get(self.section, 'api_token')
        self.endpoint = ("https://monit-grafana.cern.ch/api/datasources/"
                         "proxy/10189/monit_prod_puppetdb_raw_fact*/_search")
        self.headers = {'Content-Type': 'application/json',
                        'Authorization': self.authorization}

    def _run_queries(self):
        logger.debug('run es_puppetdb')

        hvs = self.get_hvs_per_os()
        for data in hvs:
            for release in data['os_release']['buckets']:
                self.produce_metric(
                    metric='hypervisors_per_os_release',
                    headers={
                        'os_name': data['key'],
                        'release': release['key']
                    },
                    values={
                        'count': release['distinct_count']['value']
                    }
                )

        service_vms = self.get_service_vms_per_os()
        for data in service_vms:
            for release in data['os_release']['buckets']:
                self.produce_metric(
                    metric='service_vms_per_os_release',
                    headers={
                        'os_name': data['key'],
                        'release': release['key']
                    },
                    values={
                        'count': release['distinct_count']['value']
                    }
                )

        hostgroups_data = self.get_os_release_per_hostgroup()
        for hg in hostgroups_data:
            for os in hg['os_name']['buckets']:
                for release in os['os_release']['buckets']:
                    self.produce_metric(
                        metric='os_release_per_hostgroup',
                        headers={
                            'hostgroup': hg['key'],
                            'os_name': os['key'],
                            'release': release['key']
                        },
                        values={
                            'count': release['distinct_count']['value']
                        }
                    )

    def get_data(self, query_string):
        """Query data from ElasticSearch (Grafana Proxy).

        Args:
            query_string (string): query to ES

        Raises:
            Exception: unsuccessful response code

        Returns:
            response: data from ES
        """
        query = json.dumps({
            "query": {
                "bool": {
                    "filter": [
                        {
                            "query_string": {
                                "query": query_string,
                                "analyze_wildcard": True
                            }
                        },
                        {
                            "range": {
                                "metadata.timestamp": {
                                    "format": "strict_date_optional_time",
                                    "gte": "now-12h"
                                }
                            }
                        }
                    ]
                }
            },
            "aggs": {
                "os_name": {
                    "terms": {
                        "field": "data.os.name"
                    },
                    "aggs": {
                        "os_release": {  # noqa: F601
                            "terms": {
                                "field": "data.os.release.major"
                            },
                            "aggs": {  # noqa: F601
                                "distinct_count": {
                                    "cardinality": {
                                        "field": "data.hostname"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })

        verify = False
        response = requests.get(
            url=self.endpoint,
            headers=self.headers,
            data=query,
            verify=verify,
            timeout=60)
        response.raise_for_status()
        return response.json()['aggregations']['os_name']['buckets']

    def get_data_per_hostgroup(self, query_string):
        """Query data from ElasticSearch (Grafana Proxy).

        Args:
            query_string (string): query to ES

        Raises:
            Exception: unsuccessful response code

        Returns:
            response: data from ES
        """
        query = json.dumps({
            "query": {
                "bool": {
                    "filter": [
                        {
                            "query_string": {
                                "query": query_string,
                                "analyze_wildcard": True
                            }
                        },
                        {
                            "range": {
                                "metadata.timestamp": {
                                    "format": "strict_date_optional_time",
                                    "gte": "now-12h"
                                }
                            }
                        }
                    ]
                }
            },
            "aggs": {
                "hostgroup": {
                    "terms": {
                        "field": "data.hostgroup_0",
                        "size": 100
                    },
                    "aggs": {
                        "os_name": {
                            "terms": {
                                "field": "data.os.name"
                            },
                            "aggs": {
                                "os_release": {
                                    "terms": {
                                        "field": "data.os.release.major"
                                    },
                                    "aggs": {
                                        "distinct_count": {
                                            "cardinality": {
                                                "field": "data.hostname"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })

        verify = False
        response = requests.get(
            url=self.endpoint,
            headers=self.headers,
            data=query,
            verify=verify,
            timeout=60)
        response.raise_for_status()

        return response.json()['aggregations']['hostgroup']['buckets']

    def get_hvs_per_os(self):
        query_string = ("data.fename: \"Cloud Infrastructure\" AND "
                        "data.hostgroup: /cloud_compute\\/level2\\/.*/ AND "
                        "data.is_virtual: False")
        return self.get_data(query_string)

    def get_service_vms_per_os(self):
        query_string = ("data.fename: \"Cloud Infrastructure\" AND "
                        "data.hostgroup: /cloud_.*/ AND "
                        "data.is_virtual: True")
        return self.get_data(query_string)

    def get_os_release_per_hostgroup(self):
        query_string = ("data.fename: \"Cloud Infrastructure\"")
        return self.get_data_per_hostgroup(query_string)
