import logging

from dblogger.helpers.cache import NonExistentProject
from dblogger.producer.base import BaseCloud
from octaviaclient.api.v2 import octavia

logger = logging.getLogger("dblogger")


class Loadbalancer(BaseCloud):
    def __init__(self, queue, section, configs, cache):
        """Initialize octavia producer."""
        super(Loadbalancer, self).__init__(queue, section, configs, cache)

    def _run_queries(self):
        logger.debug(
            'begin to run loadbalancer for region %s',
            self.region
        )
        self.session = self.get_session(cloud=self.region)
        self.endpoint = self.get_endpoints_per_service(
            "load-balancer",
            self.session,
            region=self.region)[0]['url']
        oc = octavia.OctaviaAPI(session=self.session, endpoint=self.endpoint)
        oc_lb = oc.load_balancer_list()
        oc_lb = oc_lb['loadbalancers']
        lbs = {}
        for d in oc_lb:
            project_id = d['project_id']
            provisioning_status = d['provisioning_status']
            good_statuses = [
                "ACTIVE",
                "PENDING_CREATE",
                "PENDING_DELETE",
                "PENDING_UPDATE"
            ]
            if project_id in lbs and provisioning_status in good_statuses:
                lbs[project_id] += 1
            elif provisioning_status in good_statuses:
                lbs[project_id] = 1
        # HA-Proxy metric file generation per loadbalancer
        lbids = {}
        values = {'data': []}
        for lb in oc_lb:
            lbids[lb['id']] = lb['project_id']
        for id, project_id in lbids.items():
            project_metadata = self.cache.get_project_metadata(project_id)
            if project_metadata['chargegroup'] != 'unknown':
                values['data'].append({
                    'id': id,
                    'projectid': project_id,
                    'chargegroup': project_metadata['chargegroup'],
                    'chargegroupname':
                        project_metadata
                        ['chargegroup_data']['name'].replace(' ', '-'),
                    'orgunit':
                        project_metadata['chargegroup_data']['org_unit'],
                })
        self.produce_metric(
            metric='haproxy-metrics',
            headers={
                'region': self.region,
            },
            values=values,
        )
        # Load balancer metrics
        for project_id, count in lbs.items():
            quota = oc.quota_show(project_id).get('load_balancer')
            try:
                project_metadata = self.cache.get_project_metadata(project_id)
            except NonExistentProject:
                continue  # Ignore, the project doesn't exist

            if project_metadata['chargegroup'] != 'unknown':
                self.produce_metric(
                    metric='loadbalancer_quota_per_project',
                    headers={
                        'project_type': project_metadata['project_type'],
                        'project_name': project_metadata['project_name'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                        'org_unit':
                            project_metadata['chargegroup_data']['org_unit'],
                        'region': self.region,
                        'project_id': project_id
                    },
                    values={
                        'quota': int(quota),
                        'usage': int(count)
                    }
                )
