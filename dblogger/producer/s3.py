import json
import logging

from dblogger.helpers.cache import NonExistentProject
from dblogger.producer.base import Base
from radosgw.connection import RadosGWAdminConnection

# configure logging
logger = logging.getLogger("dblogger")


class S3(Base):
    def __init__(self, queue, section, configs, cache):
        """Initialize s3 producer."""
        super(S3, self).__init__(queue, section, configs, cache)
        self.rgw = RadosGWAdminConnection(
            host=self.configs.get(section, 'host'),
            port=self.configs.get(section, 'port'),
            access_key=self.configs.get(section, 'access_key'),
            secret_key=self.configs.get(section, 'secret_key'),
            aws_signature=self.configs.get(section, 'aws_signature'),
            is_secure=self.configs.get(section, 'is_secure'),
        )

        logger.debug('run s3')

        project_ids = self.cache.get_tagged_projects(f"{self.region}_s3_quota")

        for project_id in project_ids:
            (buckets_used, size_used) = (
                self._get_s3_project_usage(
                    project_id=project_id, region=self.region)
            )
            (buckets_total, size_total) = (
                self._get_s3_project_quota(
                    project_id=project_id, region=self.region)
            )
            try:
                project_metadata = self.cache.get_project_metadata(
                    project_id)
            except NonExistentProject:
                continue  # Ignore, the project doesn't exist

            if project_metadata['chargegroup'] != 'unknown':
                self.produce_metric(
                    metric='s3_buckets_quota_per_project',
                    headers={
                        'region': self.region,
                        'project_id': project_id,
                        'project_name': project_metadata['project_name'],
                        'project_type': project_metadata['project_type'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                    },
                    values={
                        'quota': buckets_total,
                        'usage': buckets_used
                    }
                )

                self.produce_metric(
                    metric='s3_gb_quota_per_project',
                    headers={
                        'region': self.region,
                        'project_id': project_id,
                        'project_name': project_metadata['project_name'],
                        'project_type': project_metadata['project_type'],
                        'chargerole': project_metadata['chargerole'],
                        'chargegroup': project_metadata['chargegroup'],
                        'chargegroup_name':
                            project_metadata['chargegroup_data']['name'],
                    },
                    values={
                        'quota': size_total,
                        'usage': size_used
                    }
                )

    def _get_s3_project_usage(self, project_id, region):
        try:
            user = self.rgw.get_user(uid=project_id, stats=True)
            if user:
                return (
                    len(list(user.get_buckets())),
                    round(user.stats.size_kb_actual / 1048576))
            else:
                return (0, 0)
        except Exception:
            logger.info("Failed to get usage for project_id: %s", project_id)

    def _get_s3_project_quota(self, project_id, region):
        try:
            user = self.rgw.get_user(uid=project_id, stats=True)
            buckets = user.max_buckets
            quota_user = self.rgw.get_quota(uid=project_id,
                                            quota_type='user')
            if isinstance(quota_user, str):
                quota_user = json.loads(quota_user)
            size_kb = quota_user['max_size_kb']
            return buckets, int(size_kb / 1048576)
        except Exception:
            logger.info("Failed to get quota for project_id: %s", project_id)
