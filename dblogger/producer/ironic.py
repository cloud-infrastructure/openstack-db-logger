import logging

from datetime import datetime
from datetime import timezone
from sqlalchemy import func
from sqlalchemy import Integer
from sqlalchemy import not_
from sqlalchemy import select

from dblogger.helpers.cache import NonExistentProject
from dblogger.producer.base import BaseDB

logger = logging.getLogger("dblogger")


class Ironic(BaseDB):

    def __init__(self, queue, section, configs, cache):
        """Initialize ironic producer."""
        super(Ironic, self).__init__(queue, section, configs, cache)
        # Create table mapping
        self.Nodes = self.db.map_table('nodes')
        self.Conductors = self.db.map_table('conductors')

    def _run_queries(self):
        logger.debug(
            'run ironic for cell %s',
            self.configs.get(self.section, 'cell')
        )

        # Generate ironic data
        cores, memory = self.cores_memory_total().pop()
        self.produce_metric(
            metric="ironic_cores_memory_total_per_cell",
            headers={
                "region": self.region,
                "cell": self.configs.get(self.section, 'cell'),
            },
            values={
                'cores': int(cores) if cores else 0,
                'memory': int(memory) if memory else 0,
            })

        nodes, _ = self.nodes_total().pop()
        self.produce_metric(
            metric="ironic_nodes_per_cell",
            headers={
                "region": self.region,
                "cell": self.configs.get(self.section, 'cell'),
            },
            values={
                "count": int(nodes) if nodes else 0,
            })

        for power_state, num_nodes in self.nodes_per_power_state():
            self.produce_metric(
                metric="ironic_nodes_per_power_state_per_cell",
                headers={
                    "region": self.region,
                    "cell": self.configs.get(self.section, 'cell'),
                    "power_state": power_state
                },
                values={
                    "count": int(num_nodes)
                })

        for provision_state, conductor_group, num_nodes \
                in self.nodes_per_provision_state():
            self.produce_metric(
                metric="ironic_nodes_per_provision_state_per_cell",
                headers={
                    "region": self.region,
                    "cell": self.configs.get(self.section, 'cell'),
                    "provision_state": provision_state,
                    "conductor_group": conductor_group
                },
                values={
                    "count": int(num_nodes)
                })

        for maintenance, num_nodes in self.nodes_in_maintenance():
            self.produce_metric(
                metric="ironic_nodes_in_maintenance_per_cell",
                headers={
                    "region": self.region,
                    "cell": self.configs.get(self.section, 'cell'),
                    "maintenance": "true" if maintenance else "false"
                },
                values={
                    "count": int(num_nodes)
                })

        for conductor, num_nodes in self.nodes_per_conductor():
            self.produce_metric(
                metric="ironic_nodes_per_conductor_per_cell",
                headers={
                    "region": self.region,
                    "cell": self.configs.get(self.section, 'cell'),
                    "conductor": conductor
                },
                values={
                    "count": int(num_nodes)
                })

        self.produce_metric(
            metric="ironic_nodes_not_inspected_per_cell",
            headers={
                "region": self.region,
                "cell": self.configs.get(self.section, "cell")
            },
            values={
                "count": int(self.nodes_not_inspected())
            }
        )

        # Calculate delivery metrics
        utcnow = datetime.now(timezone.utc)

        details = self.nodes_per_resource_class_and_owner()
        for rclass, owner, nodes, cores, memory, disk, date in details:
            try:
                metadata = self.cache.get_project_metadata(owner)
                project_type = metadata['project_type']
                project_name = metadata['project_name']
                chargerole = metadata['chargerole']
                chargegroup = metadata['chargegroup']
                chargegroup_name = metadata['chargegroup_data']['name']
            except NonExistentProject:
                project_type = ''
                project_name = ''
                chargerole = ''
                chargegroup = ''
                chargegroup_name = ''

            if date:
                install_date = datetime(
                    date.year,
                    date.month,
                    date.day).replace(tzinfo=timezone.utc)
            else:
                logger.warning("No install_date for resource class %s",
                               rclass)
                install_date = utcnow

            core_count = nodes * cores if cores else 0
            memory_count = nodes * memory if memory else 0
            disk_count = nodes * disk if disk else 0

            self.produce_metric(
                metric="ironic_nodes_per_resourceclass_and_owner",
                headers={
                    "region": self.region,
                    "cell": self.configs.get(self.section, 'cell'),
                    "resource_class": rclass,
                    'project_type': project_type,
                    'project_name': project_name,
                    'chargerole': chargerole,
                    'chargegroup': chargegroup,
                    'chargegroup_name': chargegroup_name,
                },
                values={
                    "count": int(nodes),
                    "cores": int(core_count),
                    "memory": int(memory_count),
                    "disk": int(disk_count),
                    "age": int((utcnow - install_date).total_seconds()),
                })

        for rclass, used, total in self.nodes_per_resource_class_usage():
            self.produce_metric(
                metric="ironic_nodes_per_resourceclass_usage",
                headers={
                    "region": self.region,
                    "cell": self.configs.get(self.section, 'cell'),
                    "resource_class": rclass,
                },
                values={
                    "count": int(used),
                    "total": int(total),
                })

        logger.debug(
            'end run ironic for cell %s',
            self.configs.get(self.section, 'cell')
        )

    def cores_memory_total(self):
        # SELECT SUM(properties -> "$.cpus") AS cores,
        #        SUM(properties -> "$.memory_mb") AS memory
        # FROM nodes;
        return (
            self.db.query(
                func.sum(func.json_extract(self.Nodes.properties, "$.cpus")),
                func.sum(func.json_extract(self.Nodes.properties,
                                           "$.memory_mb"))
            ).all()
        )

    def nodes_total(self):
        # SELECT COUNT(id), 0
        #   FROM nodes;
        return (
            self.db.query(
                func.count(self.Nodes.id),
                0
            ).all()
        )

    def nodes_per_power_state(self):
        # SELECT power_state, COUNT(id)
        #   FROM nodes
        #   GROUP BY power_state;
        return (
            self.db.query(
                self.Nodes.power_state,
                func.count(self.Nodes.id)
            )
            .group_by(self.Nodes.power_state)
            .all()
        )

    def nodes_per_provision_state(self):
        #   SELECT provision_state, conductor_group, COUNT(id)
        #   FROM nodes
        #   GROUP BY provision_state, conductor_group;
        return (
            self.db.query(
                self.Nodes.provision_state,
                self.Nodes.conductor_group,
                func.count(self.Nodes.id)
            )
            .group_by(self.Nodes.provision_state)
            .group_by(self.Nodes.conductor_group)
            .all()
        )

    def nodes_in_maintenance(self):
        # SELECT maintenance, COUNT(id)
        #   FROM nodes
        #   GROUP BY maintenance;
        return (
            self.db.query(
                self.Nodes.maintenance,
                func.count(self.Nodes.id)
            )
            .group_by(self.Nodes.maintenance)
            .all()
        )

    def nodes_not_inspected(self):
        # SELECT COUNT(*)
        #   FROM nodes
        #   WHERE inspection_finished_at IS NULL
        #       AND provision_state != "inspecting";
        return (
            self.db.query(func.count(self.Nodes.id))
            .filter(self.Nodes.inspection_finished_at == None)  # noqa
            .filter(self.Nodes.provision_state != "inspecting")
            .scalar()
        )

    def nodes_per_conductor(self):
        # SELECT conductors.hostname, COUNT(*)
        #   FROM nodes
        #   LEFT JOIN conductors ON nodes.conductor_affinity = conductors.id
        #   GROUP BY nodes.conductor_affinity;
        return (
            self.db.query(
                self.Conductors.hostname,
                func.count(self.Conductors.id)
            )
            .join(self.Nodes,
                  self.Conductors.id == self.Nodes.conductor_affinity)
            .group_by(self.Nodes.conductor_affinity)
            .all()
        )

    def nodes_per_resource_class_and_owner(self):
        # SELECT resource_class,
        #   owner,
        #   SUM(num) AS num,
        #   MAX(cpus) AS cpus,
        #   MAX(memory_mb) AS memory_mb,
        #   MAX(local_gb) AS local_gb,
        #   MIN(install_date) AS install_date
        # FROM (
        #  SELECT n.resource_class,
        #        n.`owner`,
        #        COUNT(*) AS num,
        #        CAST(JSON_EXTRACT(n.properties, '$.cpus')
        #             AS UNSIGNED) AS cpus,
        #        CAST(JSON_EXTRACT(n.properties, '$.memory_mb')
        #             AS UNSIGNED) AS memory_mb,
        #        CAST(JSON_EXTRACT(n.properties, '$.local_gb')
        #             AS UNSIGNED) AS local_gb,
        #        DATE(n.created_at) AS install_date
        #  FROM nodes n
        #  WHERE n.`resource_class` NOT IN (
        #        'BAREMETAL_P1_RALLY',
        #        'BAREMETAL_ENROLLMENT',
        #        'BAREMETAL_AUTO_DISCOVERY')
        #  GROUP BY n.resource_class,
        #        n.`owner`,
        #        CAST(JSON_EXTRACT(n.properties, '$.cpus') AS UNSIGNED),
        #        CAST(JSON_EXTRACT(n.properties, '$.memory_mb') AS UNSIGNED),
        #        CAST(JSON_EXTRACT(n.properties, '$.local_gb') AS UNSIGNED),
        #        DATE(n.created_at)
        #  ) AS DATA GROUP BY DATA.resource_class, DATA.owner
        subquery = (
            select(
                self.Nodes.resource_class,
                self.Nodes.owner,
                func.count(self.Nodes.id).label('count'),
                func.cast(func.json_extract(
                    self.Nodes.properties, '$.cpus'),
                    Integer).label('cpus'),
                func.cast(func.json_extract(
                    self.Nodes.properties, '$.memory_mb'),
                    Integer).label('memory_mb'),
                func.cast(func.json_extract(
                    self.Nodes.properties, '$.local_gb'),
                    Integer).label('local_gb'),
                func.date(self.Nodes.created_at).label('install_date'),
            ).filter(not_(self.Nodes.resource_class.in_(
                ['BAREMETAL_P1_RALLY',
                 'BAREMETAL_ENROLLMENT',
                 'BAREMETAL_AUTO_DISCOVERY']))
            ).group_by(
                self.Nodes.resource_class,
                self.Nodes.owner,
                func.cast(func.json_extract(
                    self.Nodes.properties, '$.cpus'), Integer),
                func.cast(func.json_extract(
                    self.Nodes.properties, '$.memory_mb'), Integer),
                func.cast(func.json_extract(
                    self.Nodes.properties, '$.local_gb'), Integer),
                func.date(self.Nodes.created_at)
            ).subquery()
        )
        return (
            self.db.query(
                subquery.columns.resource_class,
                subquery.columns.owner,
                func.sum(
                    subquery.columns.count).label('count'),
                func.max(
                    subquery.columns.cpus).label('cpus'),
                func.max(
                    subquery.columns.memory_mb).label('memory_mb'),
                func.max(
                    subquery.columns.local_gb).label('local_gb'),
                func.min(
                    subquery.columns.install_date).label('install_date')
            ).group_by(
                subquery.columns.resource_class,
                subquery.columns.owner
            ).all()
        )

    def nodes_per_resource_class_usage(self):
        #  SELECT
        #    nodes.resource_class,
        #    MAX(COALESCE(used.used,0)) AS used,
        #    COUNT(id) AS total
        #  FROM nodes
        #  LEFT OUTER JOIN (
        #    SELECT
        #      resource_class,
        #      COUNT(id) AS used
        #    FROM nodes
        #    WHERE nodes.provision_state='active'
        #    GROUP BY nodes.resource_class
        #  ) AS used ON nodes.resource_class = used.resource_class
        #  WHERE nodes.resource_class NOT IN (
        #    'BAREMETAL_P1_RALLY',
        #    'BAREMETAL_ENROLLMENT',
        #    'BAREMETAL_AUTO_DISCOVERY')
        #  GROUP BY nodes.resource_class
        subquery = (
            select(
                self.Nodes.resource_class,
                func.count(self.Nodes.id).label('used'),
            ).filter(
                self.Nodes.provision_state == 'active'
            ).group_by(
                self.Nodes.resource_class
            ).subquery()
        )
        return (
            self.db.query(
                self.Nodes.resource_class,
                func.max(func.coalesce(
                    subquery.columns.used, 0)).label('used'),
                func.count(self.Nodes.id).label('total')
            ).join(
                subquery,
                self.Nodes.resource_class == subquery.columns.resource_class,
                isouter=True
            ).filter(not_(self.Nodes.resource_class.in_(
                ['BAREMETAL_P1_RALLY',
                 'BAREMETAL_ENROLLMENT',
                 'BAREMETAL_AUTO_DISCOVERY']))
            ).group_by(
                self.Nodes.resource_class
            ).all()
        )
