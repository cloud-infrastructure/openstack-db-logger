import logging

from dblogger.producer.base import BaseDB
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client
from sqlalchemy import func

logger = logging.getLogger("dblogger")


class Keystone(BaseDB):

    def __init__(self, queue, section, configs, cache):
        """Initialize keystone producer."""
        super(Keystone, self).__init__(queue, section, configs, cache)
        # Get keystone client connection
        self.keystone_client = self._get_keystone_client(
            self.configs.get(self.section, 'api_url'),
            self.configs.get(self.section, 'api_user'),
            self.configs.get(self.section, 'api_pass'),
            self.configs.get(self.section, 'api_tenant'))

        # Create table mapping
        self.Project = self.db.map_table('project')
        # Create revocation_event mapping
        self.Revocation = self.db.map_table('revocation_event')

    def _get_keystone_client(self, auth_url, user, password, project_name):
        auth = v3.Password(auth_url=auth_url,
                           username=user,
                           password=password,
                           project_name=project_name,
                           user_domain_id="default",
                           project_domain_id="default")
        sess = session.Session(auth=auth)
        return client.Client(session=sess)

    def _run_queries(self):
        logger.debug('run keystone')
        # Generate service metrics
        user_count = len(
            self.keystone_client.users.list(
                domain='default'
            )
        )
        self.produce_metric(
            metric="users_total",
            headers={
                'region': self.region,
            },
            values={
                "count": int(user_count)
            }
        )

        # SELECT COUNT(id) FROM revocation_event
        revocation_count = self.db.query(
            func.count(self.Revocation.id).label("count")).scalar()

        self.produce_metric(
            metric="revocations_total",
            headers={
                'region': self.region,
            },
            values={
                "count": int(revocation_count)
            }
        )

        projects = self.keystone_client.projects.list(
            domain='default',
            enabled='True'
        )

        self.produce_metric(
            metric="projects_total",
            headers={
                'region': self.region,
            },
            values={
                "count": int(len(projects))
            }
        )

        types = {}
        for project in projects:
            if hasattr(project, 'type'):
                if project.type not in types:
                    types[project.type] = 1
                else:
                    types[project.type] += 1

        for t in types.keys():
            self.produce_metric(
                metric="projects_per_type",
                headers={
                    'region': self.region,
                    'project_type': t,
                },
                values={
                    "count": int(types[t])
                }
            )
        logger.debug('end run keystone')
