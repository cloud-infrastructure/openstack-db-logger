import configparser
import datetime
import logging
import re

from dblogger.producer.base import BaseCloud
from dblogger.producer.base import BaseDB
from neutronclient.v2_0 import client as neutron_client
from sqlalchemy import func

logger = logging.getLogger("dblogger")


class Neutron(BaseCloud, BaseDB):

    def __init__(self, queue, section, configs, cache):
        """Initialize neutron producer."""
        super(Neutron, self).__init__(queue, section, configs, cache)

        # Create table mapping
        self.Ports = self.db.map_table('ports')
        self.agents = self.db.map_table('agents')

        # Config parameters
        try:
            self.time = self.configs.get(self.section, 'agent_down_time')
        except configparser.NoOptionError:
            self.time = 300  # by default

    def _run_queries(self):
        logger.debug('run neutron')
        # IP availability per subnet
        for subnet in self.list_ip_availability(self.region):
            available = int(subnet['total_ips']) - int(subnet['used_ips'])
            self.produce_metric(
                metric="ip_availability_per_subnet",
                headers={
                    'subnet_id': subnet['subnet_id'],
                    'cell': subnet['cell'],
                    'region': self.region,
                    'ip_service': subnet['ip_service'],
                    'secondary_service': subnet['subnet_name'],
                    'ip_version': subnet['ip_version']
                },
                # We convert to str because some IPs are bigger than
                # max int size and cannot be inserted in the DB
                values={
                    'total': subnet['total_ips'],
                    'used': subnet['used_ips'],
                    'available': available
                }
            )
        # Ports number
        self.produce_metric(
            metric="ports_total",
            headers={
                'region': self.region
            },
            values={
                "count": int(self.network_ports_total())
            }
        )

        # Ports per status
        for status, num in self.network_ports_status():
            self.produce_metric(
                metric="ports_per_status",
                headers={
                    'status': status,
                    'region': self.region
                },
                values={
                    'count': int(num)
                }
            )

        # Agents dead
        self.produce_metric(
            metric="agents_per_status",
            headers={
                'status': 'DEAD',
                'region': self.region
            },
            values={
                "count": int(self.agents_dead_total())
            }
        )

        # Agents up and working
        self.produce_metric(
            metric="agents_per_status",
            headers={
                'status': 'UP',
                'region': self.region
            },
            values={
                "count": int(self.agents_up_and_working_total())
            }
        )

        # Agents disabled
        self.produce_metric(
            metric="agents_per_status",
            headers={
                'status': 'DISABLED',
                'region': self.region
            },
            values={
                "count": int(self.agents_down_total())
            }
        )
        logger.debug('end run neutron')

    def network_ports_total(self):
        # SELECT COUNT(id) FROM ports;
        return (self.db.query(
            func.count(self.Ports.id).label("count")).
            scalar())

    def network_ports_status(self):
        # SELECT status, COUNT(id) FROM ports GROUP BY status;
        return (self.db.query(
            self.Ports.status,
            func.count(self.Ports.id).label("count")).
            group_by(self.Ports.status).all())

    def agents_dead_total(self):
        # SELECT COUNT(id) AS agents_dead
        # FROM agents WHERE TIMESTAMPDIFF(SECOND, heartbeat_timestamp,
        # utc_time()) >= 2 AND admin_state_up = 1;
        tim_threshld = (datetime.datetime.utcnow()
                        - datetime.timedelta(seconds=int(self.time)))
        tmp_data = (self.db.query(
            func.count(self.agents.id).label("agents_dead"))
            .filter((self.agents.heartbeat_timestamp) < tim_threshld)
            .filter((self.agents.admin_state_up) == 1).
            scalar())
        return tmp_data

    def agents_up_and_working_total(self):
        # SELECT admin_state_up, COUNT(id) AS agents_state_up
        # FROM agents WHERE TIMESTAMPDIFF(SECOND, heartbeat_timestamp,
        # utc_time()) < 2 AND admin_state_up = 1;
        tim_threshld = (datetime.datetime.utcnow()
                        - datetime.timedelta(seconds=int(self.time)))
        tmp_data = (self.db.query(
            func.count(self.agents.admin_state_up).
            label("agents_up_and_working"))
            .filter(self.agents.heartbeat_timestamp >= tim_threshld)
            .filter(self.agents.admin_state_up == 1).
            scalar())
        return tmp_data

    def agents_down_total(self):
        # SELECT count(*) FROM agents WHERE (admin_state_up=0);
        tmp_data = (self.db.query(
            func.count(self.agents.admin_state_up).label("agents_down"))
            .filter(self.agents.admin_state_up == 0).
            scalar())
        return tmp_data

    # NeutronClient
    def show_neutron_subnet(self, subnet_id, region):
        self.session = self.get_session(cloud=region)
        nc = neutron_client.Client(session=self.session,
                                   region_name=region)
        subnet = nc.show_subnet(subnet_id)
        return subnet['subnet'] if 'subnet' in subnet else subnet

    def get_ip_service(self, nc, subnets):
        list_subnets = []
        for cluster in nc.list_clusters()['clusters']:
            for subnet in subnets:
                if subnet['subnet_id'] in cluster['subnets']:
                    subnet['ip_service'] = cluster['name']
                    list_subnets.append(subnet)
        return list_subnets

    def split_subnets_by_cell(self, subnets):
        list_subnets = []
        for subnet in subnets:
            if len(subnet['cell']) == 1:
                subnet['cell'] = subnet['cell'][0]
                list_subnets.append(subnet)
            elif len(subnet['cell']) > 1:
                for cell in subnet['cell']:
                    subnet['cell'] = cell
                    list_subnets.append
            else:
                list_subnets.append(subnet)
        return list_subnets

    def list_ip_availability(self, region):
        self.session = self.get_session(cloud=region)
        nc = neutron_client.Client(session=self.session,
                                   region_name=region)
        ip_av = {}
        ip_av = nc.list_network_ip_availabilities()
        if ip_av and ip_av['network_ip_availabilities']:
            subnets_av = (
                ip_av['network_ip_availabilities'][0]['subnet_ip_availability']
            )
        else:
            logger.warning('No networks were found')
            return []
        subnets_cell = []
        subnets_no_cell = []
        for data in subnets_av:
            if re.findall('-V6$', data['subnet_name']):
                data['subnet_name'] =\
                    data['subnet_name'].replace('-V6', '')
            subnet_data = self.show_neutron_subnet(data['subnet_id'], region)
            if not subnet_data['tags']:
                logger.warning(
                    'Subnet %s is not mapped to any cell' %
                    data['subnet_id']
                )
                data['cell'] = ''
                data['ip_service'] = ''
                subnets_no_cell.append(data)
            else:
                data['cell'] = subnet_data['tags']
                subnets_cell.append(data)
        subnets_cell = self.get_ip_service(nc, subnets_cell)
        final_list = subnets_no_cell + subnets_cell
        return self.split_subnets_by_cell(final_list)
