import logging

from dblogger.producer.base import BaseDB
from sqlalchemy import func

logger = logging.getLogger("dblogger")


class Mistral(BaseDB):

    def __init__(self, queue, section, configs, cache):
        """Initialize mistral producer."""
        super(Mistral, self).__init__(queue, section, configs, cache)
        self.Action = self.db.map_table('action_definitions_v2')
        self.Workflow = self.db.map_table('workflow_definitions_v2')
        self.Cron = self.db.map_table('cron_triggers_v2')
        self.Workbooks = self.db.map_table('workbooks_v2')
        self.Environments = self.db.map_table('environments_v2')
        self.WorkflowExecutions = self.db.map_table('workflow_executions_v2')
        self.TaskExecutions = self.db.map_table('task_executions_v2')
        self.ActionExecutions = self.db.map_table('action_executions_v2')

    def _run_queries(self):
        logger.debug('run mistral')
        self.produce_metric(
            metric="mistral_actions_total",
            headers={
                'region': self.region
            },
            values={
                "count": int(self.total_actions())
            }
        )
        self.produce_metric(
            metric="mistral_crons_total",
            headers={
                "region": self.region
            },
            values={
                "count": int(self.total_crons())
            }
        )
        self.produce_metric(
            metric="mistral_workflows_total",
            headers={
                "region": self.region
            },
            values={
                "count": int(self.total_workflows())
            }
        )
        self.produce_metric(
            metric="mistral_workbooks_total",
            headers={
                "region": self.region
            },
            values={
                "count": int(self.total_workbooks())
            }
        )
        self.produce_metric(
            metric="mistral_environments_total",
            headers={
                "region": self.region
            },
            values={
                "count": int(self.total_environments())
            }
        )

        for row in self.total_task_executions_per_state():
            self.produce_metric(
                metric="mistral_task_executions_per_state",
                headers={
                    "region": self.region,
                    "state": row.state
                },
                values={
                    "count": int(row.count)
                }
            )

        for row in self.total_action_executions_per_state():
            self.produce_metric(
                metric="mistral_action_executions_per_state",
                headers={
                    "region": self.region,
                    "state": row.state
                },
                values={
                    "count": int(row.count)
                }
            )

        for row in self.total_workflow_executions_per_state():
            self.produce_metric(
                metric="mistral_workflow_executions_per_state",
                headers={
                    "region": self.region,
                    "state": row.state
                },
                values={
                    "count": int(row.count)
                }
            )
        logger.debug('end run mistral')

    def total_actions(self):
        return (self.db.query(
            func.count(self.Action.id).label("count")).scalar())

    def total_workflows(self):
        return (self.db.query(
            func.count(self.Workflow.id).label("count")).scalar())

    def total_crons(self):
        return (self.db.query(
            func.count(self.Cron.id).label("count")).scalar())

    def total_workbooks(self):
        return (self.db.query(
            func.count(self.Workbooks.id).label("count")).scalar())

    def total_environments(self):
        return (self.db.query(
            func.count(self.Environments.id).label("count")).scalar())

    def total_task_executions_per_state(self):
        return (
            self.db.query(
                func.count(self.TaskExecutions.state).label("count"),
                self.TaskExecutions.state.label("state")
            )
            .group_by(self.TaskExecutions.state)
            .all()
        )

    def total_action_executions_per_state(self):
        return (
            self.db.query(
                func.count(self.ActionExecutions.state).label("count"),
                self.ActionExecutions.state.label("state")
            )
            .group_by(self.ActionExecutions.state)
            .all()
        )

    def total_workflow_executions_per_state(self):
        return (
            self.db.query(
                func.count(self.WorkflowExecutions.state).label("count"),
                self.WorkflowExecutions.state.label("state")
            )
            .group_by(self.WorkflowExecutions.state)
            .all()
        )
