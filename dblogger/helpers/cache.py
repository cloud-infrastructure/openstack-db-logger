import json
import logging
import multiprocessing
import multiprocessing.managers
import requests
import time

from dblogger.storage.sql import SQL

logger = logging.getLogger("dblogger")


class CacheThread(multiprocessing.Process):
    def __init__(self, cache):
        """Initialize cache."""
        super(CacheThread, self).__init__()
        self.cache = cache

    def run(self):
        while True:
            time.sleep(3600)
            self.cache.refresh_cache()
            logger.info("Waiting 3600 seconds until the next refresh")


class CacheManager(multiprocessing.managers.BaseManager):
    pass


class NonExistentProject(Exception):
    pass


class Cache(object):
    """Cache that stores the project metadata.

    It will be a keystone cache that will store metadata like name, accounting
    group or project type. The cache will perform a period query to be
    refreshed and it will be used by the different components that only have
    project id in their database.
    """

    def __init__(self, section, configs):
        """Initialize cache for project metadata."""
        self.section = section
        self.configs = configs
        self.db = SQL(self.configs.get(self.section, 'connection'))
        self.chargegroups_file = self.configs.get(
            self.section, 'chargegroups')
        self.Project = self.db.map_table('project')
        self.s3_project = self.db.map_table('project_tag')
        # Initialize the cache
        self.chargegroups = self._get_all_chargegroups()
        self.projects = self._get_all_projects()
        self.s3_projects = self.get_tagged_projects('s3quota')
        self.deleted_projects = []
        logger.info("Initialized cache with {0} project ids".format(
                    len(self.projects)))

    def _get_all_projects(self):
        all_projects = (
            self.db.query(self.Project.id, self.Project.name,
                          self.Project.extra)
                .filter(self.Project.domain_id == 'default')
                .all()
        )

        projects = {}
        for project in all_projects:
            # Metadata is stored in JSON in the DB
            extra_json = json.loads(project.extra)

            # Metadata for this project id
            projects[project.id] = {
                'project_name': project.name,
                'project_type': extra_json.get('type', 'unknown'),
                'chargerole': extra_json.get('chargerole', 'unknown'),
                'chargegroup': extra_json.get('chargegroup', 'unknown'),
                'chargegroup_data': self.chargegroups.get(
                    extra_json.get('chargegroup', 'unknown'),
                    'unknown')
            }
        return projects

    def get_tagged_projects(self, tag):
        all_tagged_projects = (
            self.db.query(self.s3_project.project_id)
                .filter(self.s3_project.name == tag)
                .all()
        )
        tagged_projects = []
        for project in all_tagged_projects:
            tagged_projects.append(project[0])

        return tagged_projects

    def _get_all_chargegroups(self):
        url = 'https://gar.cern.ch/public/list_full'
        verify = False
        try:
            if self.chargegroups_file:
                with open(self.chargegroups_file, 'r', encoding='utf-8') as f:
                    data = f.read()
            else:
                response = requests.get(
                    url,
                    verify=verify,
                    timeout=60)
                response.raise_for_status()
                data = response.text
            # Filter out the ones that are disabled and only
            # pass the property we are using
            chargegroups = {
                ch['uuid']: {'name': ch['name'], 'org_unit': ch['org_unit']}
                for ch in json.loads(data)['data']
                if ch['name'] != 'unknown'
            }

        except requests.exceptions.HTTPError as e:
            logger.error("Error executing HTTP request. "
                         "Response code %s", e)
            chargegroups = {}
        except Exception:
            chargegroups = {}

        return chargegroups

    def get_project_metadata(self, project_id):
        if project_id in self.deleted_projects:
            raise NonExistentProject

        if project_id not in self.projects:
            # If the project is not in the cache, we get directly the
            # information from the DB. This should be only called in case of
            # new projects created
            self.projects[project_id] = self._retrieve_metadata(project_id)

        return self.projects[project_id]

    def _retrieve_metadata(self, project_id):
        """Get the metadata for the specified project id."""
        project = (
            self.db.query(self.Project)
            .filter(self.Project.id == project_id)
            .filter(self.Project.domain_id == 'default')
            .one_or_none()
        )
        if project is not None:
            extra_json = json.loads(project.extra)
            return {
                'project_name': project.name,
                'project_type': extra_json.get('type', 'unknown'),
                'chargerole': extra_json.get('chargerole', 'unknown'),
                'chargegroup': extra_json.get('chargegroup', 'unknown'),
                'chargegroup_data': self.chargegroups.get(
                    extra_json.get('chargegroup', 'unknown'),
                    'unknown')
            }
        else:
            self.deleted_projects.append(project_id)
            logger.debug("Adding %s to the list of non-existing projects",
                         project_id)
            raise NonExistentProject

    def refresh_cache(self):
        """Update cache with Keystone fresh data."""
        logger.info("Refreshing cache...")

        self.chargegroups = self._get_all_chargegroups()
        logger.info("Refreshed cache with {0} chargegroups".format(
                    len(self.chargegroups)))
        self.projects = self._get_all_projects()
        logger.info("Refreshed cache with {0} project ids".format(
                    len(self.projects)))
        self.s3_projects = self.get_tagged_projects('s3quota')
        logger.info("Refreshed cache with {0} s3 project ids".format(
                    len(self.s3_projects)))


CacheManager.register('Cache', Cache)
