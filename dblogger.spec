Name:  dblogger
Version: 3.51.4
Release: 3%{?dist}
Summary: Tool for accounting resources in openstack reading from the DBs
Source0: %{name}-%{version}.tar.gz
BuildArch: noarch
Group: CERN/Utilities
License: ASL 2.0
URL: https://gitlab.cern.ch/cloud-infrastructure/openstack-db-logger

%{?systemd_requires}
BuildRequires: systemd
BuildRequires: python3-devel
BuildRequires: python3-setuptools
BuildRequires: python3-pbr
Requires: python3-kazoo
Requires: python3-sqlalchemy
Requires: python3-PyMySQL
Requires: python3-keystoneclient
Requires: python3-keystoneauth1
Requires: python3-radosgw-admin
Requires: python3-os-client-config
Requires: python3-octaviaclient
Requires: python3-pyyaml
Requires: python3-boto3
Requires: python3-neutronclient
Requires: python-neutronclient-cern
Requires: logrotate
%description
Tool for accounting resources in OpenStack reading from the DBs

%prep
%autosetup -p1 -n %{name}-%{version}

%build
%py3_build

%install
%py3_install

%{__install} -d -m 755 %{buildroot}%{_sysconfdir}/dblogger
%{__install} -p -D -m 644 etc/chargegroups.json %{buildroot}%{_sysconfdir}/dblogger/chargegroups.json
%{__install} -p -D -m 644 etc/dblogger.conf %{buildroot}%{_sysconfdir}/dblogger/dblogger.conf
%{__install} -p -D -m 644 etc/logging.yml %{buildroot}%{_sysconfdir}/dblogger/logging.yml
%{__install} -p -D -m 644 etc/dblogger.logrotate %{buildroot}%{_sysconfdir}/logrotate.d/dblogger
%{__install} -p -D -m 644 etc/dblogger.service %{buildroot}%{_unitdir}/dblogger.service
%{__rm} -rf %{buildroot}/%{_prefix}%{_sysconfdir}
%{__install} -m 755 -d %{buildroot}/var/log/dblogger

%clean
%{__rm} -rf %{buildroot}

%post
%systemd_post dblogger.service

%preun
%systemd_preun dblogger.service

%postun
%systemd_postun_with_restart dblogger.service

%files
%attr(0755, root, root) %{_bindir}/dblogger
%{python3_sitelib}/dblogger
%{python3_sitelib}/dblogger-*-py*.egg-info/
%dir %attr(0755, root, root) %{_sysconfdir}/dblogger
%attr(0755, root, root) %{python3_sitelib}/dblogger/cmd/main.py
%dir %attr(0755, root, root) %{_localstatedir}/log/dblogger
%config(noreplace) %attr(0644, root, root) %{_sysconfdir}/dblogger/dblogger.conf
%config %attr(0644, root, root) %{_sysconfdir}/dblogger/chargegroups.json
%config(noreplace) %attr(0644, root, root) %{_sysconfdir}/dblogger/logging.yml
%config %attr(0644, root, root) %{_sysconfdir}/logrotate.d/dblogger
%{_unitdir}/dblogger.service


%changelog
* Fri Feb 14 2025 Jose Castro Leon <jose.castro.leon@cern.ch> 3.51.4-3
- Add ram measurements for each processor models

* Fri Feb 14 2025 Jose Castro Leon <jose.castro.leon@cern.ch> 3.51.4-2
- Bring back filebackend and store one file per region

* Fri Dec 13 2024 Richard Bachmann <richard.bachmann@cern.ch> 3.51.4-1
- [OS-18391] Give file consumers a dedicated process to avoid concurrency issues

* Tue Oct 22 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 3.51.3-1
- [OS-18049] Find S3 projects by new tag pattern "<region>_s3_quota"

* Thu Oct 17 2024 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> 3.51.2-1
- [OS-18349] Fix dblogger Manila quota metrics per project per type

* Thu Aug 29 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.51.1-4
- Add ironic_nodes_per_resourceclass_usage metric

* Thu Aug 15 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.51.1-3
- Fix pep8 issues
- Add metric with nodes per ironic environment

* Thu Aug 15 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.51.1-2
- Change metric name to ironic_nodes_per_resourceclass_and_owner
- Replace metrics by nodes per resource class and owner

* Tue Aug 13 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.51.1-1
- Change to multiple consumers
- Fix issues with empty size of backup
- Add metric for nodes_per_delivery

* Mon Aug 12 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.50.1-2
- Use proper source file for logging.yml

* Mon Aug 12 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.50.1-1
- Wait timeout to avoid double counting of metrics
- Add utils function to allow debugging in a multiprocess setup
- Add measurement to calculate details per delivery based on ironic information
- Add aggregate_age metric to calculate age of deliveries
- Use logging.yml instead of hardcoded config in dblogger

* Tue Jul 16 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.49.3-1
- Fix issue with empty shares
- Add region support in all components
- Remove producers for heat and magnum
- Set some defaults if there is no data

* Tue Jul 09 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.49.2-2
- Change to utc logging
- Use allowlist in tox.ini

* Tue Jun 25 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.49.2-1
- Add local file chargegroups to handle unavailability of service costing API

* Fri May 03 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.49.1-1
- Addd conductor group in baremetal provision state metric

* Fri Mar 08 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.49.0-2
- Fix enabled calculation for aggregates
- Build only for el9

* Thu Mar 07 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.49.0-1
- Add enabled field in hypervisors_X_per_aggregate metrics

* Fri Feb 02 2024 Jose Castro Leon <jose.castro.leon@cern.ch> 3.48-1
- Handle the case in which network_ip_availabilities is empty

* Thu Nov 30 2023 Spyridon Trigazis <spyridon.trigazis@cern.ch> 3.47-1
- Account all non ERROR or DELETED lbs

* Fri Oct 20 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 3.46-1
- Build for yoga release
- Fix warnings for pytest and fix tests on py3.11
- Quota change in loadbalancers after migration from TF to octavia

* Thu Jun 29 2023 Domingo Rivera Barros <driverab@cern.ch> 3.45-3
- Add new packages to setup.cfg

* Thu Jun 29 2023 Domingo Rivera Barros <driverab@cern.ch> 3.45-2
- OS-16560: Refactoring of nova producer and add usage metrics per aggregates

* Thu May 04 2023 Domingo Rivera Barros <driverab@cern.ch> 3.45-1
- Rebuild for Alma8 and RHEL8
- Add coverage report into the pipeline

* Mon Apr 03 2023 Domingo Rivera Barros <driverab@cern.ch> 3.44-6
- OS-16742: Increase size to get all the hostgroups
- OS-16742: Add OS release per hostgroup metrics

* Tue Feb 14 2023 Domingo Rivera Barros <driverab@cern.ch> 3.44-5
- Fix metric count value

* Tue Feb 14 2023 Domingo Rivera Barros <driverab@cern.ch> 3.44-4
- OS-16523: Fix os versions metrics to avoid duplicate values

* Wed Nov 09 2022 Domingo Rivera Barros <driverab@cern.ch> 3.44-3
- Support multiple OS
- Passing region as parameter

* Tue Nov 08 2022 Domingo Rivera Barros <driverab@cern.ch> 3.44-2
- Revert data type conversion

* Mon Oct 24 2022 Domingo Rivera Barros <driverab@cern.ch> 3.44-1
- Add metric to count undeleted entries in cell DBs

* Thu Oct 20 2022 Domingo Rivera Barros <driverab@cern.ch> 3.43-1
- OS-16237: Metric to show ip availability per subnet

* Thu Sep 22 2022 Domingo Rivera Barros <driverab@cern.ch> 3.42-3
- Remove quotes from metric headers

* Thu Sep 22 2022 Domingo Rivera Barros <driverab@cern.ch> 3.42-2
- OS-16220: Add new metric 'hypervisors_per_processor_per_cell'

* Wed Sep 07 2022 Domingo Rivera Barros <driverab@cern.ch> 3.42-1
- OS-16026: Add es_puppetdb producer to generate OS release metrics

* Wed Jun 29 2022 Barnabas Busa <barnabas.busa@cern.ch> 3.41-1
- Haproxy metrics to S3 buckets

* Fri Jun 17 2022 Domingo Rivera Barros <driverab@cern.ch> 3.40-2
- Add missing python-octaviaclient

* Fri Jun 17 2022 Domingo Rivera Barros <driverab@cern.ch> 3.40-1
- Add missing python3-os-client-config to spec file

* Fri Jun 17 2022 Domingo Rivera Barros <driverab@cern.ch> 3.39-1
- Refactor get_tagged_projects so it can be used for other purposes

* Fri Mar 18 2022 Domingo Rivera Barros <driverab@cern.ch> 3.38-1
- Delete unused metric 'mistral_events_total'

* Tue Feb 22 2022 Domingo Rivera Barros <driverab@cern.ch> 3.37-1
- Adding chargegroup_name to manila and cinder metrics
- Adding metric container_clusters_per_project to magnum producer

* Thu Feb 10 2022 Domingo Rivera Barros <driverab@cern.ch> 3.36-1
- Filter out unknown chargegroup names

* Wed Jan 19 2022 Domingo Rivera Barros <driverab@cern.ch> 3.35-1
- Fix s3 metrics producer

* Thu Jan 13 2022 Domingo Rivera Barros <driverab@cern.ch> 3.34-4
- Fix repositories in gitlab-ci
- Add missing python3-radosgw-admin package to spec file

* Wed Dec 01 2021 Domingo Rivera Barros <driverab@cern.ch> 3.34-3
- Fixes for s3 metrics generation

* Tue Nov 30 2021 Domingo Rivera Barros <driverab@cern.ch> 3.34-2
- Add missing s3 configuration

* Tue Nov 30 2021 Domingo Rivera Barros <driverab@cern.ch> 3.34-1
- Add s3 metrics producer

* Wed Nov 03 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.33-1
- Add central cache for chargegroups

* Thu Oct 14 2021 Domingo Rivera Barros <driverab@cern.ch> 3.32-1
- Add parameter 'kind' to distinguish between virtual and dedicated resources

* Tue Oct 12 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.31-1
- Handle multiple ironic cells

* Fri Oct 01 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.30-2
- Fix bug with tuple while retrieving ironic cores and data

* Fri Oct 01 2021 Domingo Rivera Barros <driverab@cern.ch> 3.30-1
- Add metric about total cores and memory of ironic machines
- Add new class NovaBaremetal

* Tue Sep 28 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.29-1
- Remove accounting group after chargegroup transition
- Change share_type by type in manila share

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.28-3
- Initial rebuild for el8s

* Tue Mar 9 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.28-2
- Set default avz_zone to nova on non shared cells

* Mon Mar 8 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.28-1
- Add availability zone on hypervisor_detail measurements

* Mon Mar 1 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.27-2
- Fix issue with log rotation

* Thu Feb 25 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.27-1
- Add multiple consumers backend with allowlist and denylist

* Tue Jan 19 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.26-2
- Last minute fixes on region configuration

* Tue Jan 19 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.26-1
- Add region tag in all nova produced metrics

* Fri Jan 15 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 3.25-1
- Fix group_by on magnum cluster_size_per_coe and cluster_nodes_per_coe_per_version
- Fix group_by on manila quotas and quotas_per_type
- Fix group_by on glance images_per_type

* Fri Dec 11 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.24-1
- Add fileshares_per_status metric

* Thu Sep 17 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.23-1
- Fix python3 compatibility issues in magnum producer

* Mon May 25 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.22-2
- Fix python3 compatibility issues in magnum producer

* Mon May 25 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.22-1
- Fix python3 compatibility issues
- Add python3-PyMySQL as a dependency

* Thu May 07 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.21-2
- Initial rebuild for CentOS 8

* Fri Mar 13 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.21-1
- Add chargegroup and chargerole metadata for new accounting

* Tue Mar 03 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.20-1
- Generate hypervisor detailed metrics

* Wed Feb 26 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 3.19-2
- Change main method to avoid local command clashes

* Mon Feb 24 2020 Ricardo Rocha <ricardo.rocha@cern.ch> 3.19-1
- Update magnum metrics for Train release

* Mon May 20 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 3.18-1
- Major rewrite of the tool and addition of test framework

* Thu Apr 04 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 3.17-2
- Remove urllib3 ssl validation warnings during execution

* Fri Mar 29 2019 Jose Castro Leon <jose.castro.leon@cern.ch> 3.17-1
- [OS-8763] Collect real coe version

* Fri Mar 01 2019 Ricardo Rocha <ricardo.rocha@cern.ch> 3.16-1
- Add region param and header to neutron producer

* Thu May 17 2018 Luis Piguerias <luis.pigueiras@cern.ch> 3.15-1
- [OS-6657] Include fake usage to make quota generation success

* Mon May 14 2018 Luis Pigueiras <luis.pigueiras@cern.ch> 3.14-1
- [OS-6476] Stop relying on top db for queries

* Thu Mar 15 2018 Luis Pigueiras <luis.pigueiras@cern.ch> 3.13-1
- [OS-6128] Add manila quotas measurements
- [OS-5082] Add heat stack delete complete producer

* Tue Feb 20 2018 Luis Pigueiras <luis.pigueiras@cern.ch> 3.12-1
- [OS-6066] Enforce a specific type to all generated metrics

* Fri Feb 09 2018 Luis Pigueiras <luis.pigueiras@cern.ch> 3.11-1
- [OS-5948] Add glance image information per type/status
- [OS-6007] Add mistral information on dblogger

* Mon Feb 05 2018 Luis Pigueiras <luis.pigueiras@cern.ch> 3.10-1
- [OS-5915] Fix illegal statements errors for counts

* Tue Oct 10 2017 Luis Pigueiras <luis.pigueiras@cern.ch> 3.9-1
- [OS-5297] Filter heat (non-default) projects in dblogger
- [OS-4920] Add ironic metrics in dblogger

* Wed Jun 21 2017 Luis Pigueiras <luis.pigueiras@cern.ch> 3.8-1
- [OS-4667] Add volumes_per_status metric to influx

* Thu Jun 08 2017 Luis Pigueiras <luis.pigueiras@cern.ch> 3.7-1
- [OS-4616] Ensure SQL connections finish after querying DBs
- [OS-4537] Split Magnum metrics in per_coe and per_coe_per_version

* Wed Apr 12 2017 Luis Pigueiras <luis.pigueiras@cern.ch> 3.6-1
- [OS-4458] Add more manila information and fix count queries
- Remove timeboundary from ZK partitioner

* Tue Apr 04 2017 Luis Pigueiras <luis.pigueiras@cern.ch> 3.5-1
- [OS-4218] Fix cell numbers for windows cells
- [OS-4227] Add dblogger metrics for mistral
- [OS-4441] Remove keystone tokens count from dblogger
- [OS-3967] Send project meatadata along with project ids
- Add dblogger metrics for heat

* Mon Feb 13 2017 Luis Pigueiras <luis.pigueiras@cern.ch> 3.4-1
- [OS-4087] Add coe_version to magnum coe grouping
- [OS-3772] Add ssl configuration option to dblogger
- [OS-3772] Add docker build and compose files

* Mon Jan 30 2017 Luis Pigueiras <luis.pigueiras@cern.ch> 3.3-1
- [OS-4059] Add token and revocations count
- [OS-3987] Calculate volumes total in a single measurement
- [OS-4032] Fix users count in Keystone producer
- Add README and requirements.txt
- [OS-3945] Add fix neutron status, port, agent_down_time and default time

* Mon Dec 12 2016 Luis Pigueiras <luis.pigueiras@cern.ch> 3.2-1
- [OS-3924] Added gb volume quota per type and project
- [OS-3912] Force dblogger restart after update

* Thu Dec 08 2016 Luis Pigueiras <luis.pigueiras@cern.ch> 3.1-1
- [OS-3907] Fix created/deleted in last hour metric
- [OS-3721] Add manila share count
- [OS-3890] Include disabled hypervisors in hv metrics
- [OS-3867] Include quota usages per project
- Fix comment with correct SQL query in magnum producer
- Add information about number of nodes per coe in magnum

* Fri Dec 02 2016 Luis Pigueiras <luis.pigueiras@cern.ch> 3.0-1
- Make code flake8 compliant
- Add gitlabci job
- Improve spec file to include dependencies and systemd unit file
- Add ZK variable to specify different paths
- Fix nova cell producer error when no data in a cell
- Disable Flume consumer
- Change format of produced metrics by component

* Fri Aug 26 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> 2.1-1
- Added Keystone producer
- Added Glance producer
- Added InfluxDB consumer
- New version based on Stefano's code

* Tue May 26 2015 Wataru Takase <wataru.takase@cern.ch> 0.0-7
- Added VMs creation/deletion per tenants function (OS-1486)

* Mon May 04 2015 Wataru Takase <wataru.takase@cern.ch> 0.0-5
- Improved dblogger logrotation (OS-1220) and refactored codes

* Thu Jan 29 2015 Wataru Takase <wataru.takase@cern.ch> 0.0-4
- Isolated Keystone exception handling

* Thu Jan 08 2015 Wataru Takase <wataru.takase@cern.ch> 0.0-2
- Added catching exceptions function to dblogger

* Fri Oct 10 2014 Wataru Takase <wataru.takase@cern.ch> 0.0-1
- First version
