openstack-db-logger
===================

![pipeline status](https://gitlab.cern.ch/cloud-infrastructure/openstack-db-logger/badges/master/pipeline.svg)
![coverage](https://gitlab.cern.ch/cloud-infrastructure/openstack-db-logger/badges/master/coverage.svg?job=coverage)

Tool to fetch metrics from openstack databases.

# How to setup development environment in DBLogger

Requirements:
```
git clone https://gitlab.cern.ch/cloud-infrastructure/openstack-db-logger.git
```

Run the next commands:

```
dnf install -y python-pip python-devel mysql-devel gcc  # for MySQL-python pip dependency
pip install -r requirements.txt
```

Connect to InfluxDB with:

```
influx -host dbod-ccinflqa.cern.ch -port XXXX -username admin -password "" --ssl
```

And create the database with:

```
CREATE DATABASE yourDatabaseCreatedInInfluxdb
```

Once inside `openstack-db-logger/` we have to modify different files:

Modify the configuration file `etc/dblogger/dblogger.conf.example` for the connection

``` 
[dblogger]
interval=10 # Interval to update the queries
workers=1   # To test it's enought with only one thread
zookeeper_hosts=cci-zookeeper-01:2181,cci-zookeeper-02:2181,cci-zookeeper-03:2181
zookeeper_path=example_dblogger_test # This shouldn't clash with production zookeeper path, put a random name

[influxdb]
host=dbod-ccinflqa.cern.ch
port=xxxx
user=xxxxxxx
pass=xxxxxxx
database=yourDatabaseCreatedInInfluxdb

[neutron]
name=example_neutron_test
connection=mysql://dblogger:xxxPasswordXxxx@dbod-DBName.cern.ch:DBPort/file
```

Modify the file where you want to do queries located in `src/dblogger/producer/file`.
You should map your database as below in __init__
```
# Create table mapping
self.table = self.db.map_table('nameTable')
self.table = self.db.map_table('nameTable')
```

Add configuration flake8 to check the code:
```
flake8 src/ scripts/ setup.py
```

In order to not wait for Zookeeper to acquire a lock (since there is only one development instance you don't need to wait for other dblogger processes), make this modification only while development until OS-3963 is done (**but don't commit it!!**)
```
src/dblogger/__init__.py
    partitioner = self.kclient.SetPartitioner(
    path=self.configs.get('dblogger', 'zookeeper_path'),
    set=tuple(sections),
    time_boundary=1)
```

Time to test your queries
```
PYTHONPATH=src/ scripts/dblogger -c etc/dblogger/dblogger.conf.example
```

# Docker

You can rely on docker-compose to quickly have a test environment for dblogger.

Simply edit `dblogger.env` and set the actual `DBLOGGER_PASSWORD`, and then:

```
docker build -t gitlab-registry.cern.ch/cloud-infrastructure/openstack-db-logger .
docker-compose up
```

And that's it!
