FROM fedora:25

RUN dnf install -y \
	python-configparser \
	python-influxdb \
	python-kazoo \
	python-keystoneauth1 \
	python-keystoneclient \
	MySQL-python \
	python-sqlalchemy \
&& dnf clean all

ENV DBLOGGER_PASSWORD="nopassgiven"

ADD . /source

RUN cd /source; python setup.py install

RUN mkdir -p /var/log/dblogger

RUN mkdir -p /etc/dblogger; \
	cp /source/etc/dblogger/dblogger.conf.example /etc/dblogger/dblogger.conf

CMD bash -c "sed -i s/XXXXXXXXXX/${DBLOGGER_PASSWORD}/g /etc/dblogger/dblogger.conf; dblogger"
